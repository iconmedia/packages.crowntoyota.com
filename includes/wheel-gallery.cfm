<div class="padded gallery black text-center">
    <a id="gallery" class="anchor"></a>
    <h3 class="white font-light h2"><cfoutput><span class="red">#GetWheelImage.wheelName#</cfoutput> Gallery</span></h3>
</div>
<div class="gallery-list hover-list row collapse full-width small-up-2 medium-up-3 large-up-4">
    <cfoutput query="getWheelAltGallery">
        <div class="column photo">
            <a class="wheel-gallery-photo" href="#Application.SYSTEMIMAGEPATH#wheels/med/#vchrImage#" data-html="<h5>#GetWheelImage.vchrName#</h5><p><strong>FINISH:</strong> #vchrFinishName#</p>" >
                <span>
                    <img src="#Application.SYSTEMIMAGEPATH#wheels/thumb/#vchrThumbnail#" alt="#vchrName#" />
                </span>
            </a>
        </div>
    </cfoutput>
    <cfoutput query="getWheelGallery">
        <cfif vchrImageMed1 neq ''>
            <cfif left(trim(txtDescription),23) eq 'https://www.youtube.com'>
                <!--- CODE FOR VIDEOS --->
                <div class="column medium-4 #vchrMake# #vchrModel# #brandName# #vchrWheelName# red video" title="#vchrMake# &##10; video">
                    <a class="video red" data-html="true" title="#vchrMake# &##10; video" href="#trim(txtDescription)#">
                        <img src="#Application.SYSTEMIMAGEPATH#gallery/#vchrImage1#" alt="#vchrMake# #vchrModel# #brandName# #vchrWheelName#" />
                    </a>
                </div>
            <cfelse>
                <!--- CODE FOR IMAGES --->
                <div class="column medium-4 #vchrMake# #vchrModel# #brandName# #vchrWheelName# photo" title="#vchrMake# &##10; #vchrModel#">
                    <a class="wheel-gallery-photo" data-html="<h5>#vchrMake# &##10; #vchrModel#</h5><p><strong>WHEEL:</strong> #GetWheelImage.vchrName#<br><strong>FINISH:</strong> #vchrFinishName#</p>" title="#vchrMake# &##10; #vchrModel#" href="#Application.SYSTEMIMAGEPATH#gallery/#vchrImageLg1#">
                        <img src="#Application.SYSTEMIMAGEPATH#gallery/#vchrImageMed1#" alt="#vchrMake# #vchrModel# #brandName# #vchrWheelName#" />
                    </a>
                </div>
            </cfif>
        </cfif>
    </cfoutput>
</div>
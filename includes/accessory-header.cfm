<cfparam name="finishArray" default="#arrayNew()#">
<cfparam name="brandArray" default="#arrayNew()#">
<cfparam name="sizeArray" default="#arrayNew()#">
<cfparam name="lugArray" default="#arrayNew()#">
<cfparam name="filterArray" default="#arrayNew()#">
<cfparam name="captypeArray" default="#arrayNew()#">
<cfparam name="capsArray" default="#arrayNew()#">
<cfparam name="searchArray" default="#arrayNew()#">
<cfparam name="search" default="">
<cfquery
    name = "GetAccessoryFinish"
    datasource = "#Application.DSN#">
    SELECT DISTINCT asp.vchrShortDescription as AccFinishName 
    FROM dbo.tbl_accessSpec asp WITH (NOLOCK) 
    INNER JOIN tbl_accessoryCategory ac WITH (NOLOCK) ON ac.intAccessoryID = asp.intAccessoryID
    INNER JOIN tbl_category c WITH (NOLOCK) ON c.intID = ac.intCategoryID 
    WHERE c.intParent = <cfqueryparam cfsqltype="cf_sql_integer" value="#Application.ACCESSORYID#">
    ORDER BY vchrShortDescription
</cfquery>
<cfquery
    name = "GetAccessoryBrand"
    datasource = "#Application.DSN#">
    SELECT b.vchrName as BrandName
    FROM dbo.tbl_accessories a WITH (NOLOCK)
    INNER JOIN tbl_accessoryCategory ac WITH (NOLOCK) ON ac.intAccessoryID = a.intAccessoryID
	INNER JOIN tbl_brand b WITH (NOLOCK) ON b.intID = a.intBrandID
    WHERE ac.intCategoryID = <cfqueryparam cfsqltype="cf_sql_integer" value="#Application.ACCESSORYID#">
    ORDER BY BrandName
</cfquery>
<cfquery
    name = "GetAccessorySize"
    datasource = "#Application.DSN#">
    SELECT DISTINCT ws.decDiameter as WheelSize
    FROM tbl_AccessSpec asp WITH (NOLOCK)
    INNER JOIN tbl_wheelSpec ws WITH (NOLOCK) ON ws.vchrCapNumber = asp.vchrPartNumber
    INNER JOIN tbl_accessoryCategory ac WITH (NOLOCK) ON ac.intAccessoryID = asp.intAccessoryID
    INNER JOIN tbl_category c WITH (NOLOCK) ON c.intID = ac.intCategoryID
    WHERE c.intParent = <cfqueryparam cfsqltype="cf_sql_integer" value="#Application.ACCESSORYID#">
    AND ws.decDiameter <> 0
    ORDER BY WheelSize
</cfquery>
<cfquery
    name = "GetAccessoryLug"
    datasource = "#Application.DSN#">
    SELECT DISTINCT wi.intLugs as Lugs
    FROM tbl_AccessSpec asp WITH (NOLOCK)
    INNER JOIN tbl_wheelSpec ws WITH (NOLOCK) ON ws.vchrCapNumber = asp.vchrPartNumber
    INNER JOIN tbl_WheelImage wi WITH (NOLOCK) ON wi.intWheelID = ws.intWheelID
    INNER JOIN tbl_accessoryCategory ac WITH (NOLOCK) ON ac.intAccessoryID = asp.intAccessoryID
    INNER JOIN tbl_category c WITH (NOLOCK) ON c.intID = ac.intCategoryID
    WHERE c.intParent = <cfqueryparam cfsqltype="cf_sql_integer" value="#Application.ACCESSORYID#">
    ORDER BY Lugs
</cfquery>
<cfquery
    name = "GetAccessoryCaptype"
    datasource = "#Application.DSN#">
    SELECT c.vchrName as CapType
    FROM tbl_category c WITH (NOLOCK)
    WHERE intParent = <cfqueryparam cfsqltype="cf_sql_integer" value="#Application.ACCESSORYID#">
    ORDER BY CapType
</cfquery>
<!---<cfdump var="#GetAccessoryFinish#">
<cfabort>--->
<div id="wheel-header" class="filter-header">
    <ul class="accordion" data-accordion data-multi-expand="true" data-allow-all-closed="true" role="tablist">
        <li class="accordion-item">
            <a href="#accessory-search" role="tab" class="show-for-small-only accordion-title" id="gallery-search-heading" aria-controls="accessory-search">SEARCH</a>
            <div id="accessory-search" class="accordion-content row collapse" role="tabpanel" data-tab-content aria-labelledby="gallery-search-heading">
                <form action="/accessories/" method="get">
                    <div class="small-12 medium-2 columns"><label><span>Filter:</span></label></div>
                    <!---<div class="small-12 medium-2 columns"><label><span>View By</span></label>
                        <div id="filter-filter" class="search-selector">
                            <select class="filter" name="filter">
                                <!---<option value="">Filter By:</option>--->
                                <option value="caps" <cfif structKeyExists(url, "filter") and url.filter != '' and url.filter eq 'caps'>Selected</cfif>>Caps</option>
                                <option value="wheels" <cfif structKeyExists(url, "filter") and url.filter != '' and url.filter eq 'wheels'>Selected</cfif>>Wheels</option>
                            </select>
                        </div>
                    </div>--->
                    <div class="small-12 medium-2 columns"><label><span>Part Number</span></label>
                        <div id="filter-search" class="search-selector">
                            <input type="text" name="search" value="<cfoutput>#searchCheck(search, search)#</cfoutput>">
                            <button type="submit">Search</button>
                        </div>
                    </div>
                    <div class="small-12 medium-2 columns">
                        <div id="filter-captype" class="search-selector">
                            <label for="captype" id="captype"><span>Cap Type</span>
                                <ul>
                                    <cfoutput query="GetAccessoryCapType" group="CapType">
                                        <li><label><input type="checkbox" name="captype" value="#CapType#" #checkIfChecked(captypeArray, CapType)#> #CapType#</label></li>
                                    </cfoutput> 
                                </ul>
                            </label>
                        </div>
                    </div>
                    <!---<div class="small-12 medium-2 columns">
                        <div id="filter-brand" class="search-selector">
                            <label for="brand" id="brand"><span>Brand</span>
                                <ul>
                                    <cfoutput query="GetAccessoryBrand" group="BrandName">
                                        <li><label><input type="checkbox" name="brand" value="#BrandName#" #checkIfChecked(brandArray, BrandName)#> #BrandName#</label></li>
                                    </cfoutput> 
                                </ul>
                            </label>
                        </div>
                    </div>--->
                    <div class="small-12 medium-2 columns">
                        <div id="filter-finish" class="search-selector">
                            <label for="finish" id="finish"><span>Finish</span>
                                <ul>
                                    <cfoutput query="GetAccessoryFinish" group="AccFinishName">
                                        <li><label><input type="checkbox" name="finish" value="#AccFinishName#" #checkIfChecked(finishArray, AccFinishName)#> #AccFinishName#</label></li>
                                    </cfoutput> 
                                </ul>
                            </label>
                        </div>
                    </div>
                    <div class="small-12 medium-2 columns">
                        <div id="filter-size" class="search-selector">
                            <label for="size" id="size"><span>Size</span>
                                <ul>
                                    <cfoutput query="GetAccessorySize" group="WheelSize">
                                        <li><label><input type="checkbox" name="size" value="#WheelSize#" #checkIfChecked(sizeArray, WheelSize)#> #WheelSize#"</label></li>
                                    </cfoutput> 
                                </ul>
                            </label>
                        </div>
                    </div>
                    <div class="small-12 medium-2 columns">
                        <div id="filter-lug" class="search-selector">
                            <label for="lug" id="lug"><span>Bolt</span>
                                <ul>
                                    <cfoutput query="GetAccessoryLug" group="Lugs">
                                        <li><label><input type="checkbox" name="lug" value="#Lugs#" #checkIfChecked(lugArray, Lugs)#> #Lugs# Lug</label></li>
                                    </cfoutput> 
                                </ul>
                            </label>
                        </div>
                    </div>
                <cfif structKeyExists(url, "caps") AND url.caps != ''>
                    <div id="url-caps"><cfoutput>#urlCheck(capsArray, Caps)#</cfoutput></div>
                </cfif>
                </form>                
            </div>
        </li>
    </ul>
</div>
 
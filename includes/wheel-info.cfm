<cfoutput query="GetWheelImage" group="intWheelID">
    <div class="detail-title">
        <h2>
            <span>#getCategory.vchrName#</span><br />
            <span class="text-uppercase red">#GetWheelImage.wheelName#</span> <i class="fa fa-caret-right"></i> <span id="finish"><cfif GetWheelImage.vchrFinish neq ''>#GetWheelImage.vchrFinish#</cfif> <cfif GetWheelImage.lugList neq ''> - #GetWheelImage.lugList# Lug</cfif></span>
        </h2>
    </div>
</cfoutput>
<div class="row short padded available-sizes">
    <cfoutput>#application.generateSizesList(url.id)#</cfoutput>
</div>
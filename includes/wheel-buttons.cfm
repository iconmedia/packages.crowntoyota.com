<div class="small-12 columns details-cta">
	<div class="expanded button-group stacked-for-small" data-equalizer>
		<!--- waiting for client content, below is the placeholder --->
		<a href="http://www.liquidmetalwheel.com/dealer_search.cfm" target="_blank" class="small button" data-equalizer-watch>Where to Buy</a>
		<a href="javascript:void(0);" class="fitment-open small secondary button" data-equalizer-watch>
			<cfif application.isVehicleSelected()>
				Change Vehicle
			<cfelse>
				Fitment Lookup
			</cfif>
		</a>
		<cfif GetWheelGallery.recordcount gt 0 OR getWheelAltGallery.recordcount gt 0>
			<a href="#gallery" class="scroll-to small secondary button" data-equalizer-watch>Car Photos</a>
		</cfif>
		<a href="#tech-specs" class="scroll-to small secondary button" data-equalizer-watch>Wheel Specs</a>
		<a href="javascript:void(0);" class="small secondary button open-quote-popup" data-equalizer-watch> Request For Quote</a>
	</div>
</div>
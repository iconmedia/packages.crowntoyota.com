<div class="row">
    <div class="small-12 small-up-2 medium-up-6 large-up-8 columns additional-fitment">
    <p>More Styles that fit your truck - <a href="/wheels/?fit=1&sizeID=<cfoutput>#session.size#</cfoutput>">see all</a></p>
    <cfif GetWheelImage.recordCount gt 0>
        <cfset intMaxRows = 8>
        <cfset intStartRow = (1*intMaxRows)-(intMaxRows-1)>
        <cfoutput query="getWheelImage" group="wheelName" startrow="#intStartRow#" maxrows="#intMaxRows#">
            <cfif getWheelImage.intWheelID eq getWheelImage.intWheelID><cfset intMaxRows++></cfif>
        </cfoutput>
        <cfset intStartRow = (1*intMaxRows)-(intMaxRows-1)>
        <cfoutput query="GetWheelImage" group="WheelName" startrow="#intStartRow#" maxrows="#intMaxRows#">
            <cfif getWheelImage.intWheelID neq getWheelImage.intWheelID>
                <cfset url.finish = #GetWheelImage.intFinishID#>
                <cfset finishNum = 0>
                <cfoutput group="vchrFinish">
                    <div class="column">
                        <a href="/#Application.PrettyURL(GetWheelImage.vchrName,'wheel',GetWheelImage.intWheelID)#<cfif StructKeyExists(url, "finish")>?finish=#intFinishID#&lugs=#intLugs#<cfelseif finishNum gt 0>?finish=#intFinishID#</cfif>">
                            <div class="main hover-item">
                                <img src="#Application.SYSTEMIMAGEPATH#wheels/med/#GetWheelImage.vchrImage#" alt="#GetWheelImage.WheelName#" />
                                <div class="info">
                                    <h6 class="font-heavy text-uppercase red">
                                        #ReReplace(WheelName, "^([\d-\/]+)(.*)", "<span class='red'>\1</span> \2")#
                                    </h6>
                                </div>
                            </div>
                        </a>
                    </div>
                    <cfset finishNum++ >
                </cfoutput>
            </cfif>
        </cfoutput>
    <cfelse>
        <p class="no-results">There are no results found.</p>
    </cfif>
    </div>
</div>
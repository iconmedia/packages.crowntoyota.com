<cfoutput>
    <div id="form-right" class="small-12 medium-4 columns padded">
        <h3 class="text-uppercase">#Application.TITLE# Inc.</h3>
        <ul class="no-bullet">
            <cfif application.settings.vchrPhysicalAddress neq ''>
				<li>
					<address>
						<cfoutput>
							#application.settings.vchrPhysicalAddress#
							<br>
							#application.settings.vchrCity#, #application.settings.vchrState# #application.settings.vchrPostalCode#
						</cfoutput>
					</address>
				</li>
			</cfif>
           	<cfif Application.settings.vchrPhoneNumber neq ''>
            	<li>#Application.settings.vchrPhoneNumber#</li>
			</cfif>
            <!---<li>(F) #Application.contactFax#</li>--->
            <cfif Application.settings.vchrRFQEmail neq ''>
            	<li>#Application.settings.vchrRFQEmail#</li>
			</cfif>
        </ul>
        <img src="/images/contact-logos.jpg">
        <cfif Application.settings.vchrGoogleMapsLink neq ''>
			<a href="#Application.settings.vchrGoogleMapsLink#" target="_blank">
				Click for directions
			</a>
		</cfif>
        <ul class="short padded menu simple">
           <!--- Application.generateSocialLinksList() 
                 Outputs a list of social media links. 
				@params 
					vchrOrder varchar (optinal, default = ''): Lets you select a custom order for the list. The defautl is facebook, twitter, instagram, youtube
					vchrLiClass (optional,default = ''): Lets you set a custom class for the li element
					vchrAClass (optional,default = ''): Lets you set a custom class for the a href element
					vchrIClass (optional,default = ''): Lets you set a custom class for the <i></i> element
					vchrTarget (optional, defautl = "_blank"): set to _self to have it open on the same tab
				NOTE: social links must be set in the admin for this to work. If they haven't been set, they won't show up. 
		   ---->
           <cfoutput>#application.generateSocialLinksList('intagram,youtube,facebook')#</cfoutput>
        </ul>
    </div>
</cfoutput>

<div id="gallery-specs">
	<cfoutput>
		<h1 class="gallery-title">#GetGallery.vchrMake# #GetGallery.vchrModel#</h1>
		<p>
			<cfif GetGallery.intWheelImageID gt 0>
				<cfif GetGallery.vchrWheelName neq ''>
					<strong>WHEEL:</strong> <a href="/#Application.PrettyURL(GetGallery.vchrWheelName,'wheel',GetGallery.intWheelID)#" class="red">#GetGallery.brandName# #GetGallery.vchrWheelName#</a><br />
				<cfelse>
					<strong>WHEEL:</strong>#GetGallery.brandName# #GetGallery.vchrWheelName#<br />
				</cfif>
				<cfif GetGallery.vchrFinishName neq ''>
					<strong>FINISH:</strong> #GetGallery.vchrFinishName#<br />
				</cfif>
			<cfelse>
				<cfif GetGallery.vchrName neq ''>
					<strong>WHEEL:</strong> <a href="/#Application.PrettyURL(vchrName,'wheel',GetGallery.intWheelID)#" class="red">#GetGallery.vchrName#</a><br />
				<cfelse>
					<strong>WHEEL:</strong> #GetGallery.vchrWheelName#<br />
				</cfif>
				<cfif vchrOtherWheel neq ''>
					<strong>FINISH:</strong> #GetGallery.vchrOtherWheel#<br />
				</cfif>
			</cfif>
			<cfif GetGallery.vchrWheelSize neq ''>
				<strong>WHEEL SIZE:</strong> #GetGallery.vchrWheelSize#<br />
			</cfif>
			<cfif Trim(GetGallery.intTireID) neq "" AND GetTire.recordCount gt 0>
				<strong>TIRE:</strong> #getTire.vchrBrand# #GetTire.vchrName#<br />
			</cfif>
			<cfif GetGallery.vchrTireSize neq ''>
				<strong>TIRE SIZE:</strong> #GetGallery.vchrTireSize#<br />
			</cfif>
			<cfif GetGallery.vchrOffset neq ''>
				<strong>OFFSET:</strong> #GetGallery.vchrOffset#<br />
			</cfif>
			<cfif GetGallery.vchrSuspension neq ''>
				<strong>SUSPENSION:</strong> #GetGallery.vchrSuspension#<br />
			</cfif>
			<cfif GetGallery.txtDescription neq ''>
				<div id="PhotoCredit">
					<cfif brandID neq 567><em>Credit:</em></cfif> #GetGallery.txtDescription#
				</div>
			</cfif>
		</p>
	</cfoutput>
</div>
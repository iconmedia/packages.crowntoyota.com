<cfparam name="attributes.ogurl" default="">
<cfparam name="attributes.ogimage" default="">

<!doctype html>
<html class="no-js" lang="en">

<head>
    <cfoutput>
        <meta charset="utf-8" />
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!--- Search engine optimization --->
		<title><cfif attributes.Title neq "">#Attributes.TITLE#<cfelse>#applicaiton.settings.vchrMarketingTitle#</cfif></title>
        <meta name="description" content="<cfif attributes.description eq "">#application.settings.vchrMarketingDescription#<cfelse>#attributes.description#</cfif>" />
        <meta name="keywords" content="<cfif attributes.keywords eq "">#application.settings.vchrMarketingKeywords#<cfelse>#attributes.keywords#</cfif>" />
        <!--- Open Graph --->
		<meta property="og:url" content="<cfif attributes.ogurl eq "">#Application.SYSTEMPATH#<cfelse>#attributes.ogurl#</cfif>" />
        <meta property="og:type" content="website" />
        <meta property="og:title" content="#attributes.title#" />
        <meta property="og:description" content="<cfif attributes.description eq "">#application.settings.vchrMarketingDescription#<cfelse>#attributes.description#</cfif>" />
        <meta property="og:image" content="<cfif attributes.ogimage eq "">#Application.SYSTEMPATH#img/logo.png<cfelse>#attributes.ogimage#</cfif>" />
		<meta property="og:site_name" content="#Application.TITLE#" />
      	<!--- Favicon --->
      	<cfif application.settings.vchrFavicon neq ''>
       		<link rel="icon" type="image/png" href="#Application.SYSTEMIMAGEPATH#websites/favicon/#application.settings.vchrFavicon#">
		</cfif>
       	<!--- Google Fonts --->
       	<cfloop array="#Application.arrFontFamily#" index="font"> 
    		<link href="http://fonts.googleapis.com/css?family=#font#" rel="stylesheet" type="text/css"> 
		</cfloop>
    </cfoutput>
	<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="/css/foundation.min.css" />
	<link rel="stylesheet" href="/css/motion-ui.css">
	<link rel="stylesheet" href="/js/vendor/slick/slick.css"/>
	<link rel="stylesheet" href="/css/magnific-popup.css">
    <!--- use app.css if you do not want to use sass --->
    <link rel="stylesheet" href="/css/style.css">
    <script src="/js/vendor/modernizr.js"></script>
    <script src='https://www.google.com/recaptcha/api.js'></script>
</head>

<body>
   <cfinclude template="../../modules/views/facebook-embed.cfm">
    
<div class="gallery-thumbs bg-img bg-bottom bg-right bg-auto text-center text-uppercase" style="background-image: url(/img/featured-bg.png);">
    <h5 class="light-gray-bg black">See These Tires in Action</h5>
    <div class="light-gray-bg expanded row small-up-2 medium-up-4 large-up-5">
    <cfoutput query="getGallery" maxrows="5">
        <div class="short padded column">
            <a href="#Application.SYSTEMIMAGEPATH#gallery/#vchrImageMed1#">
            <img src="#Application.SYSTEMIMAGEPATH#gallery/#vchrImageMed1#" alt="">
            </a>
        </div>
    </cfoutput>
    </div>
</div>  
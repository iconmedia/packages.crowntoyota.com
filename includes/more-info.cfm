<!--- contact form popup ---->
<div class="white-popup mfp-hide contact-popup">
	<h4 class="text-center">Contact Us</h4>
	<form action="/ajax/connector.cfc?method=contact" method="post" id="contact-form">
		<div class="row">
			<label for="name">Name</label>
			<input type="text" name="name" required>
		</div>
		<div class="row">
			<label for="phone">Phone</label>
			<input type="text" name="phone">
		</div>
		<div class="row">
			<label for="email">Email</label>
			<input type="email" name="email" required>
		</div>
		<div class="row">
			<label for="package">Package</label>
			<cfoutput>
			<input type="text" name="package" class="contact-package" value>
			</cfoutput>
		</div>
		<div class="row">
			<div class="small-12 columns">
				<span id="check-captcha" class="primary"></span>
				<div class="g-recaptcha" data-sitekey="<cfoutput>#Application.Settings.vchrRecaptchaSiteKey#</cfoutput>"></div>
			</div>
		</div>
		<input type="submit" name="submit" value="SUBMIT" class="button check-captcha">
	</form>
</div>
    <script>window.jQuery || document.write('<script src="/js/vendor/jquery.js"><\/script>')</script>
    <script src="/js/foundation.min.js"></script>
    <script src="/js/vendor/what-input.min.js"></script>
    <script src="/js/vendor/motion-ui.js"></script>
    <script>$(document).foundation();</script>
    <script src="/js/vendor/slick/slick.min.js"></script>
    <script src="/js/vendor/jquery.magnific-popup.min.js"></script>
    <script src="//iconfigurators.com/src/jquery.appguide2.js"></script>
    <!--- This requires that jquery already be loaded --->
    <div id="fitment-popup" class="white-popup mfp-hide">
		<h4 class="text-center">Fitment Lookup</h4>
		<cfinclude template="../../modules/views/app-guide.cfm">
	</div>
    
    <cfif attributes.page eq 'home'>
        <script src="/js/vendor/jquery-embedagram.js"></script>
        <script type="text/javascript" src="/js/vendor/instafeed.js"></script>
    </cfif>
    <cfif attributes.page eq 'gallery'>
        <script src="/js/vendor/jquery.infinitescroll.min.js"></script>
    </cfif>
    <cfif attributes.page eq 'inventory-results'>
        <script src="http://listjs.com/no-cdn/list.js"></script>
    </cfif>
    <cfif attributes.page eq 'wheels'>
        <cfif StructKeyExists(url, "SizeID") AND url.SizeID gt 0 AND StructKeyExists(url, "fit") AND url.fit is "1" >
           <cfset getConfig = CreateObject("component", "models.config").getConfig(application.settings.intConfigID)>
           <script src="//ver1.iconfigurators.com/pop/fancybox/jquery.fancybox.js"></script>
           <script src="//ver1.iconfigurators.com/pop/src/popup-v2.cfm?key=<cfoutput>#getConfig.vchrKey#</cfoutput>"></script>
        </cfif>
    </cfif>    
    <script src="/js/main.js"></script>
	<cfinclude template="../../modules/views/google-analytics.cfm">
	<cfoutput>#application.footerScripts#</cfoutput>
</body>
</html>


<cfmodule template="/modules/layout.cfm"
	title="#Application.TITLE#"
	page="home"
    keywords=""
	description="">
<!-- Hero -------------------------------------->
<section class="hp-hero"></section>
<!--- Packages ------------------------------->
<section class="hp-packages" data-equalizer="package">
	<div class="row" data-equalizer="truck" data-equalize-on="medium">
		<div class="column large-4">
			<div class="hp-package">
				<div class="hp-package-image" data-equalizer-watch="truck">
					<img src="/images/Tundra-Level-3.png" />
				</div>
				<div class="hp-package-info">
					<div class="hp-package-title">
						<h1>Tundra Packages</h1>
					</div>
					<div class="hp-package-models">
						<div class="row">
						    <!--- Awaiting content from client --->
						    <!---
							<div class="column small-12"><p><span>For Models</span> (All Cabs &amp; Beds):</p></div>
							<div class="column small-6 medium-12" data-equalizer-watch="package">
								<ul>
                                    <li><span>SR</span></li>
                                    <li><span>SR5</span></li>
                                    <li><span>Limited</span></li>
								</ul>
							</div>
							--->
							<div class="column small-6 medium-12">
								<div class="hp-package-button" data-equalizer-watch="package">
									<a href="/package/?vehicle=tundra" class="button">Select</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="column large-4">
			<div class="hp-package">
				<div class="hp-package-image" data-equalizer-watch="truck">
					<img src="/images/Tacoma-Level-3.png" />
				</div>
				<div class="hp-package-info">
					<div class="hp-package-title">
						<h1>Tacoma Packages</h1>
					</div>
					<div class="hp-package-models">
						<div class="row">
                            <!--- Awaiting content from client --->
						    <!---
							<div class="column small-12"><p><span>For Models</span> (All Cabs &amp; Beds):</p></div>
							<div class="column small-6 medium-12" data-equalizer-watch="package">
								<ul>
                                    <li><span>SR</span></li>
                                    <li><span>SR5</span></li>
                                    <li><span>Limited</span></li>
								</ul>
							</div>
							--->
							<div class="column small-6 medium-12">
								<div class="hp-package-button" data-equalizer-watch="package">
									<a href="/package/?vehicle=tacoma" class="button">Select</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="column large-4">
			<div class="hp-package">
				<div class="hp-package-image" data-equalizer-watch="truck">
					<img src="/images/Camry-Level-3.png" />
				</div>
				<div class="hp-package-info">
					<div class="hp-package-title">
						<h1>Camry Packages</h1>
					</div>
					<div class="hp-package-models">
						<div class="row">
						    <!--- Awaiting content from client --->
						    <!---
							<div class="column small-12"><p><span>For Models</span> (All Cabs &amp; Beds):</p></div>
							<div class="column small-6 medium-12" data-equalizer-watch="package">
								<ul>
                                    <li><span>SR</span></li>
                                    <li><span>SR5</span></li>
								</ul>
							</div>
							--->
							<div class="column small-6 medium-12">
								<div class="hp-package-button" data-equalizer-watch="package">
									<a href="/package/?vehicle=camry" class="button">Select</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
				
	</div>
</section>
</cfmodule>

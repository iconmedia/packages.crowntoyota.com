<cfswitch expression="#thisTag.ExecutionMode#">
<cfcase value="start">

<cfinclude template = "../includes/header.cfm">

<!-- Header ------------------------------------------------>
<header class="">
    <nav class="row top-bar" role="navigation">
        <div class="top-bar-title" data-equalizer="header">
            <div class="header-subtitle hide-for-small-only" data-equalizer-watch="header">
            	<h2>#1 In the World Since 1967</h2>
            </div>
            <div class="header-logo" data-equalizer-watch="header">
            	<a href="/">
            		<!---
            		<cfoutput>
            			<img src="#Application.SYSTEMIMAGEPATH#websites/logos/#application.settings.vchrPrimaryLogo#" alt="#Application.TITLE# - #Appliation.SUBTITLE#">
					</cfoutput>
           			--->
           			<img src="/images/logo.png"/><!--- Placeholder --->
            	</a>
            </div>
        </div>
    </nav>
</header>
<cfif attributes.page eq 'home'>
    <div id="content" class="hp site-content">
<cfelse>
    <div id="content" class="int site-content">
</cfif>
</cfcase>
<cfcase value="end">

<!--- Footer ------------------------------------------------>
<footer>
    <div class="footer">
      	<section class="ft-top hide-for-small-only">
      		<div class="row">
      			<div class="column medium-6 text-cnter medium-text-left">Package Builder By:</div>
                <div class="column medium-6 text-center medium-text-right"><cfoutput><a href="#Application.SYSTEMPATH#" target="_blank">#Application.websiteURL#</a></cfoutput></div><!--- Placeholder --->
      		</div>
      	</section>
        <section class="ft-bottom">
            <div class="row" data-equalizer="footer" data-equalize-on="medium">
              	<cfoutput>
                <div class="column medium-4" data-equalizer-watch="footer">
                	<div class="ft-logo">
                		<div class="show-for-small-only">Package Builder By:</div>
                		<a href="//iconfigurators.com/" target="_blank">
                			<img src="/images/iconfigurators-logo.png" />
						</a>
					</div>
                </div>
                <div class="column medium-4 text-center" data-equalizer-watch="footer">
                	<div class="ft-phone">#Application.contactTollFree#</div>
                </div>
                <div class="column medium-4" data-equalizer-watch="footer">
                	<div class="ft-company">
                	<a href="#Application.SYSTEMPATH#" target="_blank">
                		<img src="/images/logo-footer.png" />
                		<span class="show-for-small-only">#Application.websiteURL#</span>
					</a>
					</div>
                </div>
				</cfoutput>
            </div>
        </section>
    </div>
</footer>

<cfinclude template = "../includes/footer.cfm">
</cfcase>
</cfswitch>

<cfcomponent extends="controllers.connector" hint="This is a shell to do all ajax functions in. You can add more for individual sites if you need">
	<cffunction name="init">
		<cfreturn this>
	</cffunction>
	<cffunction name="getSession" access="remote" returnformat="json">
		<cfreturn session>
	</cffunction>
	<cffunction name="contact" access="remote" hint="This is a very basic contact submit. We will add more features as needed">
		<cfcontent type="text/html">
					
		<cfif isdefined("form.submit")>
            <cfset x = getHTTPRequestData()>
            <cfset clientIP = ''>
            <cfif structKeyExists(x.headers,"X-Forwarded-For")>
				<cfset clientIP = x.headers["X-Forwarded-For"] >
            </cfif>
            <cfset recaptcha = FORM["g-recaptcha-response"] >
			<cfif len(recaptcha)>
            	<cfset recaptchaUrl = "https://www.google.com/recaptcha/api/siteverify">
				<cfif Application.Settings.vchrRecaptchaSecretKey neq ''>
					<cfset secret = "#Application.Settings.vchrRecaptchaSecretKey#">
				<cfelse>
					<cfset secret = "6LfPDAwUAAAAAEO55xYyapBFc2vGrEP31t3HWHWx">
				</cfif>
            	<cfset ipaddr = CGI.REMOTE_ADDR>
            	<cfset request_url = recaptchaUrl & '?secret=' & secret & '&response=' &recaptcha & '&remoteip' & ipaddr>

            	<cfhttp url="#request_url#" method="get" timeout="10">

           		<cfset response = deserializeJSON(cfhttp.filecontent)>
            	<cfif response.success eq "YES">

					<cfmail
						from="rfq@icon-media.com"
						to="#Application.rfqEmail#"
						replyto="#form.email#"
						subject="New Submission - #Application.TITLE# Contact Form"
						type="html">
						<cfset FieldLabels = structNew("ordered")>
						<cfset FieldLabels.name = "Name">	
						<cfset FieldLabels.phone = "Phone">
						<cfset FieldLabels.email = "Email">
						<cfset FieldLabels.package = "Package Kit">

						<cfoutput>
							<ul style="list-style:none;">
								<cfloop collection=#FieldLabels# item="value">
									<cfif StructKeyExists(Form,value ) AND StructKeyExists(FieldLabels,value )>
										<li> #FieldLabels[value]# : #Form[value]# </li>
									</cfif>
								</cfloop>
							</ul>
						</cfoutput>
					</cfmail>
					<cfif structKeyExists(form, "redirectTo")>
						<cflocation url="/#form.redirectTo#">
					<cfelse>
						<cflocation url="/thank-you.cfm">
					</cfif>
				<cfelse>
					<cflocation url="#cgi.http_referer#?error=captcha did not match">
				</cfif>
			</cfif>
		</cfif>
	</cffunction>
</cfcomponent> 
<cfparam default="" name="url.wheel" />
<cfparam default="" name="url.wheelID" />
<cfparam default="" name="url.tire" />
<cfparam default="" name="url.accessories" />

<!--- Get Wheel ---->
<cfquery
	name="getWheel"
	datasource="#Application.DSN#">
	SELECT DISTINCT w.vchrName
		,w.txtDescription
		,w.vchrImage
		,w.vchrImageLarge
		,ws.vchrPartNumber
		,(SELECT b.vchrName FROM tbl_brand b WHERE b.intID = w.intbrandID) AS BrandName
	FROM dbo.tbl_wheel w WITH (NOLOCK)
	INNER JOIN dbo.tbl_wheelSpec ws WITH (NOLOCK) ON ws.intWheelID = w.intWheelID
	WHERE ws.vchrPartNumber = <cfqueryparam cfsqltype="cf_sql_varchar" value="#url.wheel#">
	AND w.intWheelID = <cfqueryparam cfsqltype="cf_sql_integer" value="#url.wheelID#">
	ORDER BY ws.vchrPartNumber
</cfquery>
<!--- Get Tire ---->
<cfquery
	name="getTire"
	datasource="#Application.DSN#">
	SELECT t.vchrName
		,t.vchrImage
		,t.vchrImageLarge
		,t.txtDescription
		,ts.vchrPartNumber
		,(SELECT b.vchrName FROM tbl_brand b WHERE b.intID = t.intbrandID) AS BrandName
	FROM dbo.tbl_tire t WITH (NOLOCK)
	INNER JOIN dbo.tbl_tireSpec ts WITH (NOLOCK) ON ts.intTireID = t.intTireID
	WHERE ts.vchrPartNumber = <cfqueryparam cfsqltype="cf_sql_varchar" value="#url.tire#">
</cfquery>
<!--- Get Accessoreis ---->
<cfquery
	name="getAccessories"
	datasource="#Application.DSN#">
	SELECT intAccessoryID
		,vchrName
		,vchrMPN
		,vchrImageMed
		,txtDescription
		,(SELECT b.vchrName FROM tbl_brand b WHERE b.intID = a.intbrandID) AS BrandName
	FROM dbo.tbl_accessories a WITH (NOLOCK)
	WHERE a.intAccessoryID IN (<cfqueryparam cfsqltype="cf_sql_integer" list="yes" value="#url.accessories#" />)
</cfquery>

<cfoutput query="getWheel">
	<div class="column">
		<div class="package-slider-images">
			<div class="package-slider-image">
				<img src="#Application.SYSTEMIMAGEPATH#wheels/med/#vchrImage#" />
			</div>
			<div class="package-slider-description">
				<div class="row" data-equalizer="slider">
					<div class="column small-12"><div class="package-slider-description-title" data-equalizer-watch="slider"><span>#brandname#</span><br />#vchrName#</div></div>
					<div class="column small-12"><div class="package-slider-description-partnumber" data-equalizer-watch="slider">(#vchrPartNumber#)</div></div>
					<cfif len(txtDescription) gt 0>
						<div class="column small-12" >
							<div class="package-slider-description-contents">
								<p>#txtDescription#</p>
								<div class="description-expand"><a href="javascript:void(0)" class="expand">More...</a></div>
							</div>
						</div>
					</cfif>
				</div>

			</div>
		</div>
	</div>
</cfoutput>
<cfoutput query="getTire">
	<div class="column">
		<div class="package-slider-images">
			<div class="package-slider-image">
				<img src="#Application.SYSTEMIMAGEPATH#tires/med/#vchrImage#" />
			</div>
			<div class="package-slider-description">
				<div class="row" data-equalizer="slider">
					<div class="column small-12"><div class="package-slider-description-title" data-equalizer-watch="slider"><span>#brandname#</span><br />#vchrName#</div></div>
					<div class="column small-12"><div class="package-slider-description-partnumber" data-equalizer-watch="slider">(#vchrPartNumber#)</div></div>
					<cfif len(txtDescription) gt 0>
						<div class="column small-12" >
							<div class="package-slider-description-contents">
								<p>#txtDescription#</p>
								<div class="description-expand"><a href="javascript:void(0)" class="expand">More...</a></div>
							</div>
						</div>
					</cfif>
				</div>

			</div>
		</div>
	</div>
</cfoutput>
<cfoutput query="getAccessories">
	<div class="column">
		<div class="package-slider-images">
			<div class="package-slider-image">
				<img src="#Application.SYSTEMIMAGEPATH#accessories/med/#vchrImageMed#" />
			</div>
			<div class="package-slider-description">
				<div class="row" data-equalizer="slider">
					<div class="column small-12"><div class="package-slider-description-title" data-equalizer-watch="slider"><span>#brandname#</span><br />#vchrName#</div></div>
					<div class="column small-12"><div class="package-slider-description-partnumber" data-equalizer-watch="slider">(#vchrMPN#)</div></div>
					<cfif len(txtDescription) gt 0>
						<div class="column small-12" >
							<div class="package-slider-description-contents">
								<p>#txtDescription#</p>
								<div class="description-expand"><a href="javascript:void(0)" class="expand">More...</a></div>
							</div>
						</div>
					</cfif>
				</div>

			</div>
		</div>
	</div>
</cfoutput>

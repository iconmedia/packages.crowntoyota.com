<cfparam default="" name="url.country" />

<cfset country = url.country />

<cfquery name="GetStates"
	datasource="#Application.dsn#">
	SELECT [vchrState]
        ,[vchrAbbr]
        ,[vchrCountry]
    FROM [Icon_master].[dbo].[tbl_state] WITH (NOLOCK)
	WHERE vchrCountry = <cfqueryparam cfsqltype="cf_sql_varchar" value="#url.country#" />
    ORDER By vchrState
</cfquery>

<cfset response ="" />
<cfif GetStates.recordcount gt 0 >
    <cfset response ="<option value=''>Select a State</option> ">
    <cfoutput query="GetStates" >
        <cfset response = response &"<option value='#vchrAbbr#'>#vchrState#</option>">
    </cfoutput>
<cfelse>
    <cfset response = response &"<option value='N/A' Selected >N/A</option>">
</cfif>

<cfoutput>
    #response#
</cfoutput>

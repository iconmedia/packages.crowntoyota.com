<h2 class="tire-info_header heading text-uppercase">
    <span class="primary"> <cfoutput>#GetTireImage.brandName#</cfoutput> </span> 
    <span class="brand-details_finish"><cfoutput><cfif GetTireImage.vchrName neq ''> #GetTireImage.vchrName# </cfif></cfoutput></span>
</h2>
<cfif GetTireImage.txtDescription neq ''>
    <div class="brand-details">
        <div class="">
            <a href="##description" class="show-for-small-only" id="description-heading">DETAILS</a>
            <div id="description" class="">
                <p class="black-font product-paragraph info--limit"><cfoutput> #GetTireImage.txtDescription# </cfoutput></p>
                <button class="text-uppercase primary hidden info-Show"><i class="fa fa-plus-circle"></i> More </button>
                <button class="text-uppercase primary hidden info-Hide"><i class="fa fa-minus-circle"></i> Less </button>
            </div>
        </div>
    </div>
</cfif>
<div class="row short padded">
    <cfif isDefined('GetSpecs') AND GetSpecs.recordcount gt 0>
        <div class="small-12 columns">
            <p class="product-paragraph info--limit">
                <span class="text-uppercase">Available in Sizes: </span><br/>
                <cfoutput query="GetSpecs" group="vchrSize"><cfif GetSpecs.currentrow neq 1>, </cfif>#vchrSize#</cfoutput>
            </p>
            <button class="text-uppercase primary hidden info-Show"><i class="fa fa-plus-circle"></i> More </button>
            <button class="text-uppercase primary hidden info-Hide"><i class="fa fa-minus-circle"></i> Less </button>
        </div>
    </cfif>
</div>
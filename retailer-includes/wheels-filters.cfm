<cfif structKeyExists(url,'SizeID')>
    <cfset getFilterSizes = createObject("component", "models.wheel-filters").getSizesWithVehicle()>
    <cfset getFilterFinishes = createObject("component", "models.wheel-filters").getFinishesWithVehicle()>
    <cfset getFilterBrands = createObject("component", "models.wheel-filters").getBrandsWithVehicle()>
<cfelse>
    <cfset getFilterSizes = createObject("component", "models.wheel-filters").getSizesWithoutVehicle()>
    <cfset getFilterFinishes = createObject("component", "models.wheel-filters").getFinishesWithoutVehicle()>
    <cfset getFilterBrands = createObject("component", "models.wheel-filters").getBrandsWithoutVehicle()>
</cfif>

<div class="wheelBrand_filter clearfix" data-equalizer data-equalize-on="medium-up">
    <div class="primary-bg small-12 medium-2 column filter_header text-center " data-equalizer-watch>
        <h5 class="white filter_header--text"><cfif structKeyExists(url,'SizeID')> Filters <cfelse> Wheel Search </cfif></h5>
    </div>
    <div id="filters" class="medium-10 column end row" data-equalizer-watch>
        <cfif !structKeyExists(url,'SizeID')>
        <div class="filter_select medium-3 column text-center"  >
            <button class="button medium text-uppercase filter_button fitment-open"> Shop By Vehicle </button>
        </div>
        </cfif>
        <div class="filter_select <cfif !structKeyExists(url,'SizeID')> medium-3 <cfelse> medium-4 </cfif> column" >
            <cfif StructKeyExists(url, "sizeID") AND url.SizeID gt 0>
                <select name="sizeID" id="sizeid">
                <cfoutput query="GetFilterSizes">
                    <option value="#Int(intSizeID)#"<cfif StructKeyExists(url, "sizeID") and url.sizeID eq intSizeID> selected</cfif>>#FrontWheel# Inch Wheels</option>
                </cfoutput>
                </select>
            <cfelse>
                <select id="wheel-diam" name="diam">
                    <option value=""> Shop By Size </option>
                    <cfoutput query="GetFilterSizes">
                        <option value="#Int(decdiameter)#"<cfif StructKeyExists(url, "diam") and url.diam eq Int(decDiameter)> selected</cfif>>#decDiameter# Inch Wheels</option>
                    </cfoutput>
                </select>
            </cfif>
        </div>
        <div class="filter_select <cfif !structKeyExists(url,'SizeID')> medium-3 <cfelse> medium-4 </cfif>  column" >
            <select name="finish" id="finish" >
                <option value=""> Shop By Finish </option>
                <option value="">All</option>
                <cfoutput query="getFilterFinishes">
                    <option value="#vchrShortName#"  <cfif StructKeyExists(url, "finish") AND vchrShortName EQ url.finish > SELECTED </cfif>  >#vchrShortName#</option>
                </cfoutput>d
            </select>
        </div>
        <div class="filter_select <cfif !structKeyExists(url,'SizeID')> medium-3 <cfelse> medium-4 </cfif> column end">
            <select name="brand" id="brand">
                <option value="0"> Shop By Brand </option>
                <option value="0">All</option>
                <cfoutput query="getFilterBrands">
                <option value="#BrandID#" <cfif brandID is url.brand > SELECTED </cfif>>#vchrName#</option>
                </cfoutput>
            </select>
        </div>
    </div>
</div>
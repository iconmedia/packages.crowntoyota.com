<div class="wheel-cta">
    <cfif isDefined('wheelPrice')>
        <p class="wheel-paragraph">
            <span class="primary wheel-price"> Price Range: </span> <br/>
            <cfoutput> #wheelPrice# </cfoutput>
        </p>
    </cfif>
    <ul class="stack button-group">
        <li>
            <a href="javascript:void(0);" class="open-detail-quote-popup open-quote-popup small button button_quote" data-equalizer-watch>
                <cfif isDefined('hideButton') AND hideButton eq 0>
                    Get A Quote <br/> Contact Our Experts
                <cfelse>
                    Get Quote 
                </cfif>
            </a>
        </li>
        <cfif (isDefined('url.SizeID') AND url.SizeID gt 0) AND StructKeyExists(session, "size")>
        <li><a href="/wheels/?fit=1&sizeID=<cfoutput>#url.sizeID#</cfoutput>" class="small button" data-equalizer-watch> Change Wheel </a></li>
        </cfif>
        <cfif url.sizeID lte 0>
            <li><a href="javascript:void(0);" id="fitmentCheck" class="checkFitment-open small button button_fitment" data-equalizer-watch>
                    Will It Fit My Truck
            </a></li>
        </cfif>
        <cfif isDefined('hideButton') AND hideButton eq 0>
        <li><a href="javascript:void(0);" class="small button cart-open" data-equalizer-watch> Buy It Now </a></li>
        </cfif>
    </ul>
</div>


<!--- Fitment Check Popup --->
<div id="checkFitment-popup" class="white-popup mfp-hide" <cfoutput> data-wheelimageid="#getWheelImage.intWheelImageID#" data-finish="#getWheelImage.intFinishID#" </cfoutput>>
    <h4 class="text-center text-uppercase"> Check Fitment </h4>
    <div id="app-guide">
        <div class="row">
        <select name="years" class="vehicle-years">
            <option>Select Year</option>
        </select>
        </div>
        <div class="row">
            <select name="makes" class="vehicle-makes" disabled="disabled">
                <option>Select Make</option>
            </select>
        </div>
        <div class="row">
            <select name="models" class="vehicle-models" disabled="disabled">
                <option>Select Model</option>
            </select>
        </div>
        <div class="row">
            <select name="submodels" class="vehicle-submodels" disabled="disabled">
                <option>Select Sub Model</option>
            </select>
        </div>
        <div class="row">
            <select name="sizes" class="vehicle-sizes" disabled="disabled">
                <option>Select Size</option>
            </select>
        </div>
        <div class="expanded button-group">
            <button id="fitmentSubmit" class="button small no-margin">See Results</button>
            <button id="fitmentClear" class="button small secondary no-margin">Clear</button>
        </div>
    </div>
</div>  

<!--- Fitment Check Script --->
<cfsavecontent variable="script">
    <script>
        $('#checkFitment-popup #app-guide').appguide({
            configID: <cfoutput>#application.settings.intConfigID#</cfoutput>
            ,redirectTo: false
            ,useJson: 1
            ,saveSessionTo     : "/ajax/connector.cfc"
            ,submitCallback: function() {
                $.getJSON('/ajax/connector.cfc?method=fitmentCheck&wheelImageID=<cfoutput>#getWheelImage.intWheelImageID#</cfoutput>', function returnResults(data){
                    var html= '';
                    $.getJSON('/ajax/connector.cfc?method=getSession', function(session){
                        if(data.results > 0){
                            $('#fitmentCheckResults').html(data);
                            $.magnificPopup.open({
                                    items: {
                                    src: 		
                                    '<div id="fitmentCheckResults" class="white-popup">' +
                                    '<h4 class="text-center popup_header"> See on Vehicle - Fitment Guide </h4>' +
                                    '<div class="fitmentCheck_content text-center">' +
                                        '<p class="fitmentCheck_text"> This Wheel fits your: <br/>' +
                                        '<span class="black-font">'+session.YEAR + ' ' + session.STRMAKE + ' ' + session.STRMODEL +' </span>' +
                                        '</p>' +
                                        '<a href="?finish=<cfoutput>#getWheelImage.intFinishID#</cfoutput>&fit=1&sizeID=' + session.SIZE +'"> <button type="button" class="button uppercase fitmentCheck_Button"> continue with purchase </button> </a>' +
                                    '</div>' +
                                    '</div>',
                                    type: 'inline',
                                    modal: true
                                    }
                            });
                        }else{
                            $.magnificPopup.open({
                                    items: {
                                    src: 		
                                    '<div id="fitmentCheckResults" class="white-popup">' +
                                    '<h4 class="text-center popup_header"> See on Vehicle - Fitment Guide </h4>' +
                                    '<div class="fitmentCheck_content text-center">' +
                                        '<p class="fitmentCheck_text"> This Wheel DOES NOT fit your: <br/>' +
                                        '<span class="black-font">' +session.YEAR + ' ' + session.STRMAKE + ' ' + session.STRMODEL +'</span>' +
                                        ', but we do have other <br/>' +
                                        'vehicles that Fit:' +
                                        '</p>' +
                                        '<a href="/wheels/?fit=1&sizeID=' + session.SIZE +'"> <button type="button" class="button uppercase fitmentCheck_Button"> see wheels that do fit </button> </a>' +
                                    '</div>' +
                                    '</div>',
                                    type: 'inline',
                                    modal: true
                                    }
                            });
                        }	
                    });
                });
            }
            <cfoutput>
                <cfif structKeyExists(session, "year")>
                    ,year: #session.year#
                </cfif>
                <cfif structKeyExists(session, "make")>
                    ,make: #session.make#
                </cfif>
                <cfif structKeyExists(session, "model")>
                    ,model: "#session.model#"
                </cfif>
                <cfif structKeyExists(session, "submodel")>
                    ,submodel: "#session.submodel#"
                </cfif>
                <cfif structKeyExists(session, "size")>
                    ,size: #session.size#
                </cfif>
            </cfoutput>
        });
</script>
</cfsavecontent>
<cfset temp = application.addToFooter(script)>
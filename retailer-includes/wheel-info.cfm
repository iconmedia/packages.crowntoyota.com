<div class="wheel-info">
    <h2 class="wheel-info_header text-uppercase">
    <cfoutput>
        <span class="primary">#GetWheelImage.wheelName# </span>
        <span class="wheel-info_longfinish"><cfif GetWheelImage.vchrFinish neq ''>/ #GetWheelImage.vchrFinish#</cfif></span> <br/>
        <span class="wheel-info_lugs"> #GetWheelImage.intLugs# Lug</span>
    </cfoutput>
    </h2>
    <ul class="accordion" data-accordion data-multi-expand="true" data-allow-all-closed="true" role="tablist">
        <li class="accordion-item">
            <a href="##description" role="tab" class="show-for-small-only accordion-title" id="description-heading" aria-controls="description">DETAILS</a>
            <div id="description" class="accordion-content" role="tabpanel" data-tab-content aria-labelledby="description-heading">
                <cfif GetWheelImage.vchrDescription neq ''>
                <cfoutput query="GetWheelImage" group="intWheelID">
                    <p class="product-paragraph info--limit">#wheelDescription#</p>
                    <button class="text-uppercase primary hidden info-Show"><i class="fa fa-plus-circle"></i> More </button>
                    <button class="text-uppercase primary hidden info-Hide"><i class="fa fa-minus-circle"></i> Less </button>
                </cfoutput>
                </cfif>
                    <p class="product-paragraph wheel-sizes">
                        <span class="wheel-sizes_title text-uppercase"> Available in Sizes: </span><br/>
                        <cfoutput query="GetSpecs" group="vchrSize"><cfif GetSpecs.currentrow neq 1>, </cfif>#rereplace('#GetSpecs.vchrSize#', '0+$', '', 'ALL')#</cfoutput>
                    </p>
            </div>
        </li>
    </ul>
</div>
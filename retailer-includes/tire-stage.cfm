<div class="row images">
<cfoutput query="GetTireImage" group="intTireId">
<cfif GetAngles.recordcount gt 0>
    <div class="alt-images small-12 medium-2 column text-center"></div>
</cfif>
<div class="medium-10 columns">
    <div class="main-image text-center">
        <div>
            <a href="#Application.SYSTEMIMAGEPATH#tires/large/#vchrImageLarge#">
                <img src="#Application.SYSTEMIMAGEPATH#tires/large/#vchrImageLarge#" alt="#vchrName#" />
            </a>
        </div>
        <cfif GetAngles.recordcount gt 0>
            <cfloop query="GetAngles">
                <div>
                    <a href="#Application.SYSTEMIMAGEPATH#tires/large/#vchrImageLarge#">
                        <img src="#Application.SYSTEMIMAGEPATH#tires/large/#vchrImageLarge#" alt="#GetTireImage.vchrName#" />
                    </a>
                </div>
            </cfloop>
        </cfif>
    </div>
</div>
</cfoutput>
</div>
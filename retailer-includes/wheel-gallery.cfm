<div class="wheel-gallery">
    <cfif getWheelGallery.recordcount gt 0>
        <h2 class="wheel-gallery_header">Vehicle Gallery</h2>
        <ul class="wheel-galleryImages">
            <cfoutput query="getWheelGallery">
                <li class="wheel-galleryList">
                <cfscript> vchrTireSize =  toString( #vchrTireSize#); </cfscript>
                    <a href="##" class="galleryView-open"
                        data-car="#vchrMake# #vchrModel#"
                        data-wheelimage="#Application.SYSTEMIMAGEPATH#gallery/#vchrImageLg1#"
                        data-wsize="#vchrWheelSize#"
                        data-wheel="#vchrWheel#"
                        data-finish="#vchrFinish#"
                        data-tester="#vchrWheelSize#"
                        data-tire="#vchrTire#"
                        data-tsize="#vchrTireSize#"
                        data-suspension='<cfif vchrSuspension neq ''>#vchrSuspension#</cfif>' style="display:inline-block">
                        <div class="galleryImage" style="background-image: url('#Application.SYSTEMIMAGEPATH#gallery/#vchrImage1#');" alt="#vchrMake# #vchrModel# with #GetWheelImage.brandName# #GetWheelImage.wheelName#"></div>
                        <span class="black-font"> #vchrMake# #vchrModel# </span>
                    </a>
                </li>
            </cfoutput>
        </ul>
    </cfif>
    <button class="text-uppercase primary hidden wheel-galleryShowButton"><i class="fa fa-plus-circle"></i> More </button>
    <button class="text-uppercase primary hidden wheel-galleryHideButton"><i class="fa fa-minus-circle"></i> Less </button>
</div>  

<!--- Wheel Gallery Popup --->
<div id="galleryView-popup" class="mfp-hide gallery-popup">
    <button class="mfp-close">&#215;</button><br/>
    <img class="popup-wheelimage" src="" /><br />
    <div class="galleryView-info text-uppercase">
        <span class="popup-car"></span>
        <span class="popup-wheel"></span>
        <span class="popup-finish"></span>
        <span class="popup-wsize"></span>
        <span class="popup-tire"></span>
        <span class="popup-tsize"></span>
        <span class="popup-suspension"></span>
    </div>
</div>
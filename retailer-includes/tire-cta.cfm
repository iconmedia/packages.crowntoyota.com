<div class="tire-cta">
<cfif (isDefined('getMinTirePrice') AND getMinTirePrice.recordcount) gt 0 AND getMaxTirePrice.recordcount gt 0>
    <p class="wheel-paragraph">
        <span class="primary"> Price Range: </span> <br/>
        <cfoutput> $#getMinTirePrice.minTirePrice#  - $#getMaxTirePrice.maxTirePrice# (ea) </cfoutput>
    </p>
<cfelse>
    <span class="primary"> Price Range: </span> <br/>
    <p class="wheel-paragraph text-uppercase">
    Call For Price
    </p>
</cfif>
<ul class="stack button-group">
    <li><a href="javascript:void(0);" class="open-quote-popup small button">Get Quote</a></li>
    <li><a href="javascript:void(0);" class="fitment-open small secondary button" data-equalizer-watch>
        <cfif structKeyExists(session,"year") AND structKeyExists(session,"strMake") AND structKeyExists(session,"strModel") AND StructKeyExists(session, "size") AND session.size gt 0>
                Change Vehicle
        <cfelse>
                Find Wheels
        </cfif>
    </a>
    </li>
</ul>
</div>
<div class="wheel-stage">
<cfoutput query="GetWheelImage" group="intWheelID">
	<div class="medium-10 medium-push-2 columns">
		<div class="main-image text-center">
			<div>
				<a href="#Application.SYSTEMIMAGEPATH#wheels/xlarge/#vchrImageXLarge#">
					<img src="#Application.SYSTEMIMAGEPATH#wheels/large/#vchrImageLarge#" alt="#brandName# #wheelName#" />
				</a>
			</div>
			<cfif GetAngles.recordcount gt 0>
				<cfloop query="GetAngles">
					<div>
						<a href="#Application.SYSTEMIMAGEPATH#wheels/xlarge/#vchrImageXLarge#">
							<img src="#Application.SYSTEMIMAGEPATH#wheels/large/#vchrImageLarge#" alt="#getWheelImage.brandName# #GetWheelImage.wheelName#" />
						</a>
					</div>
				</cfloop>
			</cfif>
		</div>
	</div>
	<cfif GetAngles.recordcount gt 0>
		<div class="alt-images alt-images--retailer small-12 medium-2 medium-pull-10 column text-center">
			<div>
				<img src="#Application.SYSTEMIMAGEPATH#wheels/thumb/#vchrThumbnail#" alt="#brandName# #wheelName#" />
			</div>
			<cfloop query="GetAngles">
				<div>
					<img src="#Application.SYSTEMIMAGEPATH#wheels/thumb/#vchrThumbnail#" alt="#getWheelImage.brandName# #GetWheelImage.wheelName#" />
				</div>
			</cfloop>
		</div>
	</cfif>
</cfoutput>
</div>
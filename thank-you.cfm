<cfmodule template="/modules/layout.cfm"
	title="Contact - #Application.TITLE#"
	page="contact"
    keywords=""
	description="">

<div id="thanks">
	<div class="parallax">
        <div class="img" style="background-image: url(/images/contact-bg.jpg);"></div>
        <h1 class="padded text-uppercase white">Thank You</h1>
    </div>
	<div class="padded row text-center">
		<div class="padded small-12 columns">
	        <h3 class="text-uppercase">Your message has been submitted.</h3>
	        <cfoutput>
	            <p>
                    An #Application.TITLE2# representative will contact you in a timely manner.
                </p>
	        </cfoutput>
	    </div>
	</div>
</div>

</cfmodule>

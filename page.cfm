<cfquery
    name = "GetPage"
    datasource = "#Application.DSN#">
    SELECT vchrName
        ,vchrMarketingKeywords
        ,vchrMarketingDescription
        ,txtDescription
        ,vchrImage
    FROM tbl_content
    WHERE  1 = 1
    <cfif structKeyExists(url, "id")>
    	AND intPageID = <cfqueryparam value = "#url.id#" CFSQLType="cf_sql_integer">
    <cfelse>
    	AND intPageID = <cfqueryparam value = "#session.intPageID#" CFSQLType="cf_sql_integer">
	</cfif>
       AND intAccountID =  <cfqueryparam cfsqltype="cf_sql_integer" value="#Application.settings.intACCOUNTID#">
</cfquery>

<cfmodule template="/modules/layout.cfm"
	title="#GetPage.vchrName# - #Application.TITLE#"
	page="page"
    keywords="#GetPage.vchrMarketingKeywords#"
	description="#GetPage.vchrMarketingDescription#">

<div id="page">
    <cfoutput query="GetPage">
        <div class="parallax">
            <div class="img bg-top" style="background-image: url(#Application.SYSTEMIMAGEPATH#content/#vchrImage#);"></div>
            <cfoutput>
                <h1 class="text-uppercase white">#vchrName#</h1>
            </cfoutput>
        </div>
    </cfoutput>
    <div class="page-content">
        <div class="row">
            <cfoutput query="GetPage">
                <div class="small-12 columns">
                    #txtDescription#
                </div>
            </cfoutput>
        </div>
    </div>
</div>
</cfmodule>

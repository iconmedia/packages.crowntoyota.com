/* global $, Foundation, MotionUI, List */
/* ==========================================================================
   Table of Contents
   ========================================================================== */


/*
    1.   General
    ---  Product Pages  ---
    2.1  Brand Pages
    2.2  Detail Pages
    ---  Gallery Pages  ---
    3.1  Gallery Landing Page
    3.3  Gallery Detail Page
    ---  Other Pages  ---
    4.1  Home Page
    4.2  Contact Page
    4.3  Article Page
*/
$(function App() {
  /* ==========================================================================
     1. General
     ========================================================================== */
  // Cache
  var $img = $('.parallax .img');
  var $heading = $('.parallax h1');
  var scroller;
  var wh = $(window).height();
  var intBrand = 0;
  var intWheel = 0;
  var intFinish = '';
  var decDiam = '0.0';
  var decWidth = '0.0';
  var strLug = '';
  var strPCD = '';
  var intOffStart = -130;
  var intOffEnd = 130;
  var intOffStartSel = -140;
  var intOstart = 0;
  var arrReturn = [];
  var strOffResult = '';
  var inventoryOptions;

  // Newsletter Validation
  $('form.ajax-form').submit(function formRequired(event) {
    var $this = $(this);
    var $requiredFields = $this.find('input[required]');
    var fail = false;
    var message = '';

    event.preventDefault();
    $requiredFields.each(function checkRequired(index, el) {
      if ($(el).val() === '') {
        fail = true;
        message = $(el).attr('data-error');
      }
    });
    if (fail === true) {
      $('#newsletterModal .lead').text(message);
      $('#newsletterModal').foundation('open');
      return false;
    }
      // Submit via AJAX
    $.post($this.attr('action'), $this.serialize(), function submitForm(data) {
      $('#newsletterModal .lead').text(data);
      $('#newsletterModal').foundation('open');
    });
    return true;
  });


  $('.fitment-open').magnificPopup({
    items: {
      src: '#fitment-popup',
      type: 'inline'
    }
  });

 $('.checkFitment-open').magnificPopup({
    items: {
      src: '#checkFitment-popup',
      type: 'inline'
    }
  });
  
  $('#fitmentSubmit').on('click', function submitFitment() {
    var size = $('#size');
    if (size.val() && size.val() !== 'Select Size') {
      size.change();
    }
  });

  // Mobile Accordion
  if ($('.accordion').length && Foundation.MediaQuery.atLeast('medium')) {
    $('.accordion').each(function openAccordion() {
      $(this).foundation('down', $(this).find('.accordion-content').first(), true);
    });
  }

  // Scroll to section
  $('.scroll-to').on('click', function scrollToElem(event) {
    var target = $($(this).attr('href'));
    if (target.length) {
      event.preventDefault();
      $('html, body').animate({
        scrollTop: target.offset().top
      }, 1000);
    }
  });

	// Varify captcha
	$('.rfqSubmit, .check-captcha').on('click', function(){
		if (grecaptcha.getResponse() == ""){
      var response = document.getElementById('check-captcha');
      response.innerHTML = 'Captcha box not selected';
			return false;
		} else {
				return true;
		}
	});
  /* ==========================================================================
     2.1 Brand Pages
     ========================================================================== */

	// Function for Wheel Filter to change page to 1 when a brand is selected
	// on a page greater than 1
	function replaceUrlParam(url, key, value) {
    if(value == null)
        value = '';
    var pattern = new RegExp('\\b('+key+'=).*?(&|$)')
    if(url.search(pattern)>=0){
        return url.replace(pattern,'$1' + value + '$2');
    }
    return url + (url.indexOf('?')>0 ? '&' : '?') + key + '=' + value
  }

  // Wheel filters redirect
  $('#filters, #tireFilters').on('change', 'select', function (event) {
    event.preventDefault();
    // Get type of filter and current query
    var $this = $(this);
    var type = $this.attr('name');
    var query = window.location.search;
		var parts = document.location.href.split('/');

    // If this filter has already been applied, replace
    if (query.indexOf(type + '=') > -1) {
      var regex = new RegExp('(' + type + '=)([^&]+)?', 'ig');
      query = query.replace(regex, '$1' + $this.val());
    // If there are already other params
    } else if (query.indexOf('?') > -1) {
      query += '&' + type + '=' + $this.val();
    // If this is the only param
    } else {
      query += '?' + type + '=' + $this.val();
    }
    
		var elem = $this.parent().attr('id');
		// Set the page to 1
		if(window.location.href.match('page')) {
		 query = replaceUrlParam(query, page, 1);
	  }
		if ($this.parent().attr('id') == 'tireFilters') {
				if(window.location.href.match('tires/' + /\d+/) > -1){
					window.location.href = '/tires/'+query;
				}
    } else if(window.location.href.match('wheels/' + /\d+/) > -1){
				var urlSplit = document.location.href.split('/');
				var urlSection = urlSplit[4];
				if(urlSection > 0) {
						window.location.href = '/wheels/' + urlSection + '/' +query;
				} else {
					window.location.href = '/wheels/' +query;
			  }
		}else{
    window.location.href = window.location.pathname + query;
		}
  });
  
  // PARALLAX EFFECT   http://codepen.io/sallar/pen/lobfp
  // requestAnimationFrame Shim
  window.requestAnimFrame = (function animFramPoly() {
    return window.requestAnimationFrame ||
      window.webkitRequestAnimationFrame ||
      window.mozRequestAnimationFrame ||
      function animTimout(callback) {
        window.setTimeout(callback, 1000 / 60);
      };
  }());

  // Scroller
  function Scroller() {
    this.latestKnownScrollY = 0;
    this.ticking = false;
  }

  Scroller.prototype = {
    // Initialize
    init: function init() {
      window.addEventListener('scroll', this.onScroll.bind(this), false);
    },

    // Capture Scroll
    onScroll: function onScroll() {
      this.latestKnownScrollY = window.scrollY;
      this.requestTick();
    },

    // Request a Tick
    requestTick: function requestTick() {
      if (!this.ticking) {
        window.requestAnimFrame(this.update.bind(this));
      }
      this.ticking = true;
    },

    // Update
    update: function update() {
      var currentScrollY = this.latestKnownScrollY;

      // Do The Dirty Work Here
      var imgScroll = currentScrollY / 2;
      var headScroll = currentScrollY / 3;

      this.ticking = false;

      $img.css({
        transform: 'translateY(' + imgScroll + 'px)',
        '-moz-transform': 'translateY(' + imgScroll + 'px)',
        '-webkit-transform': 'translateY(' + imgScroll + 'px)'
      });

      $heading.css({
        transform: 'translateY(' + headScroll + 'px)',
        '-moz-transform': 'translateY(' + headScroll + 'px)',
        '-webkit-transform': 'translateY(' + headScroll + 'px)'
      });
    }
  };

  // Attach!
  if ($('.parallax .img').length || $('.parallax h1').length) {
    scroller = new Scroller();
    scroller.init();
  }

  // Filter Categories
  $('.brand-nav').change(function changeBrand() {
    window.location.replace($(this).val());
  });
  // Quick View Popup
  $('.quickview-open').click(function wheelsQuickview() {
    var $this = $(this);
    var finishName = $this.data('finishname');
    var wheelName = $this.data('wheelname');
    var brandName = $this.data('brandname');
    var wheelImage = $this.data('wheelimage');
    var wheelLink = $this.data('wheellink');
    var wheelSizes = $this.data('sizes');
    $('#quickview-popup')
        .find('.popup-brandname')
        .html(brandName);
    $('#quickview-popup')
        .find('.popup-wheelname')
        .html(wheelName);
    $('#quickview-popup')
        .find('.popup-wheelfinish')
        .html(finishName);
    $('#quickview-popup')
        .find('.popup-sizes')
        .html(wheelSizes);
    $('#quickview-popup')
        .find('.popup-wheelimage')
        .attr('src', wheelImage);
    $('#quickview-popup')
        .find('.popup-wheellink')
        .attr('href', wheelLink);


    $.magnificPopup.open({
      items: {
        src: '#quickview-popup',
        type: 'inline'
      }
    });
  });
// Header Filters
  $('#filter-size').change(function changeMake() {
    $('#wheel-search form').submit();
  });
  $('#filter-finish').change(function changeMake() {
    $('#wheel-search form').submit();
  });

  /* ==========================================================================
     2.2 Detail Pages
     ========================================================================== */
  // Slider and popup for main image
  function initializeMainImage() {
    $('.main-image').magnificPopup({
      delegate: 'a',
      type: 'image',
      gallery: {
        enabled: true,
        navigateByImgClick: true,
        preload: [0, 1]
      }
    }).slick({
      arrows: false,
      asNavFor: '.alt-images',
      fade: true,
      slidesToShow: 1,
      slidesToScroll: 1
    });

    $('.alt-images').slick({
      arrows: false,
      asNavFor: '.main-image',
      dots: true,
      focusOnSelect: true,
      infinite: false,
      slidesToShow: 4,
      slidesToScroll: 1
    });
  }

  initializeMainImage(); 

  // RFQ Popup
  $('.open-quote-popup').magnificPopup({
    items: {
      src: '#quote-popup',
      type: 'inline'
    }
  });

	// Gallery View Popup
  $('.galleryView-open').on('click', function () {
    for (var dataName in $(this).data()) {
      if ($(this).data()[dataName] !== '') {
        if (dataName === 'wheelimage') {
          $('#galleryView-popup img').attr('src', $(this).data()[dataName]);
        } else {
          var htmlString = '';
          htmlString = dataName;
          htmlString += ': ';
          htmlString += $(this).data()[dataName];
          htmlString += '<br/>';
          htmlString = htmlString.replace(/car: /, '').replace(/size/, ' size');
          $('.galleryView-info').find('.popup-' + dataName).html(htmlString);
        }
      } else {
        $('.galleryView-info').find('.popup-' + dataName).html('');
      }
    }
    $.magnificPopup.open({
      items: {
        src: '#galleryView-popup',
        type: 'inline'
      }
    });
  });

	// Show/Hide excess paragraph lines
  function getRows(selector) {
    var height = $(selector).height();
    var lineHeight = $(selector).css('line-height');
    var rows = height / lineHeight;
    height = parseFloat(height);
    lineHeight = parseFloat(lineHeight);
    return Math.round(rows);
  }

	// Show/Hide excess information
  $(document).ready(
    function toggleInfo() {
      if ($('.wheel-galleryImages > li').length > 6) {
        $('.wg-Show').removeClass('hidden');
      }
      if (getRows('.info--limit') > 5) {
        $('.info--limit').addClass('info--height');
        $('.info-Show').removeClass('hidden');
      }
  });

  function toggleButtons(classSelect, cssSelect, showBtnToggle, hideBtnToggle) {
    $(classSelect).toggleClass(cssSelect);
    $(showBtnToggle).toggleClass('hidden');
    $(hideBtnToggle).toggleClass('hidden');
  }

  $('.info-Show, .info-Hide').on('click', function toggleParagraph() {
    toggleButtons('.info--limit', 'info--height', '.info-Show', '.info-Hide');
  });
  $('.wg-Show, .wg-Hide').on('click', function toggleGallery() {
    toggleButtons('.wheel-galleryImages > li', 'wheel-galleryList', '.wg-Show', '.wg-Hide');
  });
	

	
	
	
	// get active
	/*$(document).ready(function(){
		activated = $('.packages .active a');
		if (activated.length > 0) {
			getVehicleImage = activated.data('image');
			getPackageNumber = activated.data('package');
			getLink = activated.data('link');
			$('.package-header-link').prop('href',getLink);
			$('.package-header-image img').prop('src',getVehicleImage);
			$('.package-slider-header span').text(getPackageNumber);
			$('.packages .active .package-button').html('On Vehicle');
			
		}
	});*/
	
	// swtich active
	$('.package-button').click(function(e){
	 	var vehicleImage = $(this).data('image');
		var packageTitle = $(this).data('packageTitle');
		var packageNumber = $(this).data('package');
		var kitLink = $(this).data('link');
		var wheelNumber = $(this).data('wheel');
		var wheelID = $(this).data('wheelid');
		var tireNumber = $(this).data('tire');
		var accessoryNumbers = $(this).data('accessories');

		e.preventDefault();
		$('.package-button').html('Show On Vehicle');
		$(this).html('On Vehicle');
		$('.package-header-link').prop('href',kitLink);
		$('.package-header-image img').prop('src',vehicleImage);
		$('.package-slider-header span').text(packageNumber);
		$('.package-package').val(packageTitle + ' ' + packageNumber);
		$.get('/ajax/packageSlider.cfm?wheel='+wheelNumber+'&wheelID='+wheelID+'&tire='+tireNumber+'&accessories='+accessoryNumbers, function getKitList(data) {
			$('.package-list-slider').slick('unslick');
			$('.package-list-slider').html(data);
			initKitSlider();
			expandSliderDesc();
		});
		$('.package').removeClass('active');
		$(this).parent().parent().addClass('active');
	});
	
	
	// package slider
	function initKitSlider() {
		$('.package-list-slider').slick({
			focusOnSelect: false,
			infinite: false,
			slidesToShow: 3,
			slidesToScroll:3,
			responsive: [
			  {
				breakpoint: 600,
				settings: {
				  dots: false,
				  slidesToScroll: 1,
				  slidesToShow: 1
				}
			  }
			]
		  });
		}
	
	initKitSlider();
	
// contact form Popup
$('.contactUs').magnificPopup({
	callbacks: {
		open: function() {
			var magnificPopup = $.magnificPopup.instance;
			var packageKit = magnificPopup.st.el.data('packagekit');
			$('.contact-package').val(packageKit);
		}
	},
	items: {
	  src: '.contact-popup',
	  type: 'inline'
	}
});

	// toggle plus/minus
	$('.activate-toggle').click(function(){
		if ($('.fa-plus-square').length > 2) {
			$(this).find('.fa-plus-square').addClass('fa-minus-square').removeClass('fa-plus-square');
		} else if ($('.fa-minus-square').length > 0){
			$(this).find('.fa-minus-square').addClass('fa-plus-square').removeClass('fa-minus-square');
		}
	});
	function expandSliderDesc() {
	$('.package-slider .expand').click(function(){
		var $this = $(this);
		var $sibling = $(this).parent().siblings('p');
		var $characterLength = $(this).parent().siblings('p').text().length;
		if($sibling.hasClass('open')){
			$sibling
				.removeClass('open');
			$this.text('More...').find('i.fa')
				.addClass('fa-ellipsis-h')
				.removeClass('fa-chevron-up');

		}else{
			$sibling
				.addClass('open');
			$this.text('Less...').find('i.fa')
				.removeClass('fa-ellipsis-h')
				.addClass('fa-chevron-up');
		}
	});
	};
	expandSliderDesc();
	
	/*$(window).resize(function(){
		if ($('.packages').length) {
			if($(window).width() < 625) {
				console.log('we be buggin');
				if($('.package-button').hasClass('button')){
					$('.package-button').removeClass('button');
				}
			} else {
				if(!$('.package-button').hasClass('button')){
					$('.package-button').addClass('button');
				}
			}
		}
	});*/
	
  /* ==========================================================================
     3.1  Gallery Landing Page
     ========================================================================== */
  $('#gallery a.video').magnificPopup({
    disableOn: 700,
    type: 'iframe',
    mainClass: 'mfp-fade',
    removalDelay: 160,
    preloader: false,
    fixedContentPos: false
  });

  /* ==========================================================================
     3.2  Gallery Detail Page
     ========================================================================== */
  $('#gallery-main-image').magnificPopup({
    delegate: 'a',
    type: 'image',
    gallery: {
      enabled: true,
      navigateByImgClick: true,
      preload: [0, 1]
    }
  });

  $('#gallery-main-image').slick({
    adaptiveHeight: false,
    arrows: false,
    asNavFor: '#gallery-thumbs',
    fade: true,
    lazyLoad: 'ondemand'
  });
  $('#gallery-thumbs').slick({
    arrows: false,
    asNavFor: '#gallery-main-image',
    focusOnSelect: true,
    infinite: false,
    slidesToShow: 5,
    responsive: [
      {
        breakpoint: 600,
        settings: {
          dots: true,
          slidesToScroll: 3,
          slidesToShow: 3
        }
      }
    ]
  });

  /* ==========================================================================
     4.1  Home Page
     ========================================================================== */
/* MOTION UI - FADE IN TRANSITIONS /////////*/
  if (Foundation.MediaQuery.atLeast('medium')) {
    $(window).scroll(function checkScroll() {
      $('.animate').each(function animateItem() {
        var $this = $(this);
        if (($this.offset().top - $(window).scrollTop()) - wh <= -350) {
          MotionUI.animateIn($this, 'fade-in', function adjustOpacity() {
            $this.css('opacity', '1');
          });
          $this.removeClass('animate');
        }
      });
    });
  }

/* MOTION UI - RANDOM TRANSITIONS /////////*/
    /*  if (Foundation.MediaQuery.atLeast('medium')) {
    $(window).scroll(function scrollPage() {
      $('.animate').each(function animateElem() {
        var $this = $(this);
        var transitions = ['slide-in-left', 'slide-in-up', 'slide-in-right', 'fade-in',
                           'hinge-in-from-right', 'hinge-in-from-bottom', 'hinge-in-from-left'];
        var randomNum = Math.floor(Math.random() * (transitions.length - 1));
        if (($this.offset().top - $(window).scrollTop()) - wh <= 0) {
          MotionUI.animateIn($this, transitions[randomNum]);
          $this.removeClass('animate');
        }
      });
    });
  }*/

  /* ==========================================================================
     4.2  Contact Page
     ========================================================================== */
  $('#vchrCountry select').change(function changeCountry() {
    $.get('/ajax/getStates.cfm?country=' + $(this).val(), function getStates(data) {
      $('#vchrState select').html(data).prop('disabled', false);
    });
  });
  $('#form-left form').submit(function checkRequired() {
    var fail = false;
    var message = '';
    var errorCount = 0;
    var name = '';
    var title = '';
    $('#form-left form [required]').each(function checkFields(index, element) {
      var $element = $(element);
      if ($element.val() === '') {
        $element.css('background', 'red');
        fail = true;
        errorCount++;
        name = $element.siblings('label').replace('*', '');
        message += name + ' is required. ';
      }
    });
    if (fail) {
      title = $(this).attr('data-title');
      $('#modal').html('<p>Form submission failed for the following reason(s):' + message + '</p>')
        .dialog({
          minHeight: 150,
          width: 300,
          modal: true,
          title: title,
          closeText: 'X',
          buttons: {
            Okay: function closeModal() {
              $(this).dialog('close');
            }
          }
        });
      return false;
    }
    return true;
  });
  /* ==========================================================================
     4.3  Inventory Page
     ========================================================================== */
  function getData(url, selector) {
    var i;
    $.get('/ajax/getData.cfm?' + url, function getInventory(data) {
      if (selector === '#offstart') {
        arrReturn = data.split(',');

        if (intOstart === 0) {
          intOffStart = isNaN(parseInt(arrReturn[0], 10)) ? -130 : parseInt(arrReturn[0], 10);
        } else {
          intOffStart = intOffStartSel + 10;
        }

        intOffEnd = isNaN(parseInt(arrReturn[1], 10)) ? 130 : parseInt(arrReturn[1], 10);
        strOffResult = '<option value="">All</option>';

        for (i = intOffStart; i <= intOffEnd; i += 10) {
          strOffResult += '<option value="' + i + '">' + i + '</option>';
          if (i >= 130) {
            break;
          }
        }

        if (intOstart === 0) {
          $(selector).html(strOffResult);
        }
        $('#offend').html(strOffResult);
      } else {
        $(selector).html(data);
      }
    });
  }

  if ($('#inventory').length) {
    $('#brand').on('change', function changeBrand() {
      var url = '';
      intBrand = ($(this).val() !== '') ? $(this).val() : 0;
      intOstart = 0;
      url = '&brand=' + intBrand;
      getData('step=1' + url, '#wheel');
      getData('step=2' + url, '#finish');
      getData('step=3' + url, '#diam');
      getData('step=4' + url, '#width');
      getData('step=5' + url, '#lugs');
      getData('step=6' + url, '#pcd');
      getData('step=7' + url, '#offstart');
    });

    $('#wheel').on('change', function changeWheel() {
      var url = '';
      intWheel = ($(this).val() !== '') ? $(this).val() : 0;
      intOstart = 0;
      url = '&brand=' + intBrand + '&wheel=' + intWheel;
      getData('step=2' + url, '#finish');
      getData('step=3' + url, '#diam');
      getData('step=4' + url, '#width');
      getData('step=5' + url, '#lugs');
      getData('step=6' + url, '#pcd');
      getData('step=7' + url, '#offstart');
    });

    $('#finish').on('change', function changeFinish() {
      var url = '';
      intFinish = ($(this).val() !== '') ? $(this).val() : '';
      url = '&brand=' + intBrand + '&wheel=' + intWheel + '&finish=' + intFinish;
      getData('step=3' + url, '#diam');
      getData('step=4' + url, '#width');
      getData('step=5' + url, '#lugs');
      getData('step=6' + url, '#pcd');
      getData('step=7' + url, '#offstart');
    });

    $('#diam').on('change', function changeDiam() {
      var url = '';
      decDiam = ($(this).val() !== '') ? $(this).val() : 0;
      intOstart = 0;
      url = '&brand=' + intBrand + '&wheel=' + intWheel + '&finish=' + intFinish +
            '&diam=' + decDiam;
      getData('step=4' + url, '#width');
      getData('step=5' + url, '#lugs');
      getData('step=6' + url, '#pcd');
      getData('step=7' + url, '#offstart');
    });

    $('#width').on('change', function changeWidth() {
      var url = '';
      decWidth = ($(this).val() !== '') ? $(this).val() : 0;
      intOstart = 0;
      url = '&brand=' + intBrand + '&wheel=' + intWheel + '&finish=' + intFinish +
            '&diam=' + decDiam + '&width=' + decWidth;
      getData('step=5' + url, '#lugs');
      getData('step=6' + url, '#pcd');
      getData('step=7' + url, '#offstart');
    });

    $('#lugs').on('change', function changeLug() {
      var url = '';
      strLug = ($(this).val() !== '') ? $(this).val() : '';
      intOstart = 0;
      url = '&brand=' + intBrand + '&wheel=' + intWheel + '&finish=' + intFinish +
            '&diam=' + decDiam + '&width=' + decWidth + '&lug=' + strLug;
      getData('step=6' + url, '#pcd');
      getData('step=7' + url, '#offstart');
    });

    $('#pcd').on('change', function changePCD() {
      var url = '';
      strPCD = ($(this).val() !== '') ? $(this).val() : '';
      intOstart = 0;
      url = '&brand=' + intBrand + '&wheel=' + intWheel + '&finish=' + intFinish +
            '&diam=' + decDiam + '&width=' + decWidth + '&lug=' + strLug + '&pcd=' + strPCD;
      getData('step=7' + url, '#offstart');
    });

    $('#offstart').on('change', function changeOffstart() {
      var url = '';
      intOffStartSel = ($(this).val() !== '') ? parseInt($(this).val(), 10) : -140;
      intOstart = 1;
      url = '&brand=' + intBrand + '&wheel=' + intWheel + '&finish=' + intFinish +
            '&diam=' + decDiam + '&width=' + decWidth + '&lug=' + strLug + '&pcd=' + strPCD +
            '&offSetStart=' + intOffStartSel;
      getData('step=7' + url, '#offstart');
    });
  }

  if ($('#inventory-results').length > 0) {
    inventoryOptions = {
      page: 5000,
      valueNames: ['partNum', 'brand', 'name', 'finish', 'diam', 'width', 'bolt', 'offset', 'qty']
    };
    /* eslint no-unused-vars: ["error", { "varsIgnorePattern": "userList" }]*/
    var userList = new List('inventoryTable', inventoryOptions);

    $('.sort').on('click', function sortItems() {
      var $this = $(this);
      var icon = 'fa-caret-up';
      if ($this.find('i').hasClass('fa-caret-up')) {
        icon = 'fa-caret-down';
      } else {
        icon = 'fa-caret-up';
      }
      $('.sort i').remove();
      $this.append('<i class="fa ' + icon + '"></i>');
    });

    $('.image_link').magnificPopup({
      type: 'image'
    });
  }
});
  /* ==========================================================================
     2.2 Accessories Pages
     ========================================================================== */

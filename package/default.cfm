<cfparam name="url.vehicle" default="" />
<cfif StructKeyExists(url,'vehicle') AND url.vehicle eq ''>
	<cflocation url="/" addToken="no" />
</cfif>
<!--- Tundra --->
<cfif StructKeyExists(url,'vehicle') AND url.vehicle eq 'tundra'>
	<cfset package.vehicleTitle = "2018 Tundra" />
	<cfset package.vehicleImage = "package/Tundra-Level-0-lrg.png" />
	<cfset package.vehicleImageStart = "package/Tundra-Level-" />
	<cfset package.vehicleImageEnd = "-lrg.png" />
	<cfset package.kit = "Tundra Package" />
	<cfset package.link0 = "//admin.iconfigurators.com/admin/configurators/popup/default.cfm?ky=211E299E3E98A7EB2536" />
	<cfset package.link1 = "//admin.iconfigurators.com/admin/configurators/popup/default.cfm?ky=211E299E3E98A7EB2536##/year=2017&model=TUNDRA&submodel=SR5&size=18%20Inch%20Wheels&wheel=Vector%20-%20D579%20Matte%20Black&currentTab=accessories&tireBrand=BFGoodrich&tire=All-Terrain%20T/A%20KO2&accessCatParentID=4371&access0=Genuine%20Toyota%20Parts%20TRD%20PRO%20SHIFT%20KNOB%20AUTOMATIC%202017%20TOYOTA%20TUNDRA-4RUNNER&access190=Genuine%20Toyota%20Parts%20EXHAUST%20TIP%202017%20TOYOTA%20TUNDRA&access220=Rigid%20Industries%20LEVEL%202%20KIT%202014-2017%20TOYOTA%20TUNDRA%20&access180=Genuine%20Toyota%20Parts%20TRD%20PRO%20GRILL%202017%20TOYOTA%20TUNDRA&color=Super%20White%20II" />
	<cfset package.link2 = "//admin.iconfigurators.com/admin/configurators/popup/default.cfm?ky=211E299E3E98A7EB2536##/year=2017&model=TUNDRA&submodel=SR5&size=18%20Inch%20Wheels&wheel=Vector%20-%20D579%20Matte%20Black&currentTab=accessories&tireBrand=BFGoodrich&tire=All-Terrain%20T/A%20KO2&accessCatParentID=4372&access0=Genuine%20Toyota%20Parts%20TRD%20PRO%20SHIFT%20KNOB%20AUTOMATIC%202017%20TOYOTA%20TUNDRA-4RUNNER&access190=Genuine%20Toyota%20Parts%20EXHAUST%20TIP%202017%20TOYOTA%20TUNDRA&access220=Rigid%20Industries%20LEVEL%202%20KIT%202014-2017%20TOYOTA%20TUNDRA%20&access180=Genuine%20Toyota%20Parts%20TRD%20PRO%20GRILL%202017%20TOYOTA%20TUNDRA&color=Super%20White%20II&access160=Genuine%20Toyota%20Parts%20PREDATOR%20TUBE%20STEP%20ASSEMBLY%202017%20TOYOTA%20TUNDRA%20&access185=Rigid%20Industries%20IGNITE%20BACKUP%20KIT,%20STANDARD%20-%202016-2017%20TACOMA-TUNDRA-4RUNNER%20" />
	<cfset package.link3 = "//admin.iconfigurators.com/admin/configurators/popup/default.cfm?ky=211E299E3E98A7EB2536##/year=2017&model=TUNDRA&submodel=SR5&size=18%20Inch%20Wheels&color=Super%20White%20II&currentTab=tires&access0=Genuine%20Toyota%20Parts%20TRD%20PRO%20SHIFT%20KNOB%20AUTOMATIC%202017%20TOYOTA%20TUNDRA-4RUNNER&access190=Genuine%20Toyota%20Parts%20EXHAUST%20TIP%202017%20TOYOTA%20TUNDRA&access180=Rigid%20Industries%20GRILLE%202014-2018%20TOYOTA%20TUNDRA%20&access160=Genuine%20Toyota%20Parts%20PREDATOR%20TUBE%20STEP%20ASSEMBLY%202017%20TOYOTA%20TUNDRA%20&access245=Rigid%20Industries%20LEVEL%203%20KIT%202014-2017%20TOYOTA%20TUNDRA%20&tireBrand=BFGoodrich&wheel=Vector%20-%20D579%20Matte%20Black&tire=All-Terrain%20T/A%20KO2" />
	<cfset package.price1 = "3,897" />
	<cfset package.price2 = "8,201" />
	<cfset package.price3 = "9,027" />
	<cfset package.AccessoriesList = "3982,7168,5940,5946,5691,5898,5900,5936,5941,5945" />
	<cfset package.excludeList1 = "5941,5945,5936,5691,5900" />
	<cfset package.excludeList2 = "5691,5900" />
	<cfset package.excludeList3 = "5946,5898" />
	<cfset package.accList1 = "3982,5946,7168,5940,5898" />
	<cfset package.accList2 = "3982,5946,7168,5940,5898,5941,5945,5936" />
	<cfset package.acclist3 = "3982,7168,5940,5941,5945,5936,5691,5900" />
	<cfset package.wheel = 7257>
	<cfset package.wheelPartNumber = 'D57918905657'>
	<cfset package.tire = 3849>
	<cfset package.tirePartNumber = '66255'>
</cfif>
<!--- Tacoma ---->
<cfif StructKeyExists(url,'vehicle') AND url.vehicle eq 'tacoma'>
	<cfset package.vehicleTitle = "2018 Tacoma" />
	<cfset package.vehicleImage = "package/Tacoma-Level-0.png" />
	<cfset package.vehicleImageStart = "package/Tacoma-Level-" />
	<cfset package.vehicleImageEnd = ".png" />
	<cfset package.kit = "Tacoma Package" />
	<cfset package.link0 = "//admin.iconfigurators.com/admin/configurators/popup/default.cfm?ky=211E299E3E98A7EB2536" />
	<cfset package.link1 = "//admin.iconfigurators.com/admin/configurators/popup/default.cfm?ky=211E299E3E98A7EB2536##/year=2017&model=TACOMA&submodel=BASE&size=16%20Inch%20Wheels&color=Super%20White%20II&wheel=PT758%20Gloss%20Black&currentTab=accessories&tireBrand=BFGoodrich&tire=All-Terrain%20T/A%20KO2&accessCatParentID=4372&access0=Genuine%20Toyota%20Parts%20WHEELS%20LOCKS%20CLEAR%20CHROME%205%20PIECES%202000-2017%20Toyota%20Alloy%20&access180=Genuine%20Toyota%20Parts%20TRD%20PRO%20GRILL%202016-2017%20TOYOTA%20TACOMA&access220=Rigid%20Industries%20FOG%20KIT%20SAE%20D/2%20LEVEL%202%20-%202016-2017%20TACOMA%20&access190=Genuine%20Toyota%20Parts%20EXHAUST%20TIP%202017%20TOYOTA%20TACOMA" />
	<cfset package.link2 = "//admin.iconfigurators.com/admin/configurators/popup/default.cfm?ky=211E299E3E98A7EB2536##/year=2017&model=TACOMA&submodel=BASE&size=16%20Inch%20Wheels&color=Super%20White%20II&wheel=PT758%20Gloss%20Black&currentTab=accessories&tireBrand=BFGoodrich&tire=All-Terrain%20T/A%20KO2&accessCatParentID=4372&access0=Genuine%20Toyota%20Parts%20TAN/BIEGE%20SEATS%20KATZKIN%202014-2017%20TOYOTA%204RUNNER/TACAMO/TUNDRA&access220=Rigid%20Industries%20FOG%20KIT%20SAE%20D/2%20LEVEL%202%20-%202016-2017%20TACOMA%20&access190=Genuine%20Toyota%20Parts%20EXHAUST%20TIP%202017%20TOYOTA%20TACOMA&access160=Genuine%20Toyota%20Parts%20PREDATOR%20TUBE%20STEP%20ASSEMBLY%202017%20TOYOTA%20TACOMA&access180=Genuine%20Toyota%20Parts%20TRD%20PRO%20GRILL%202016-2017%20TOYOTA%20TACOMA&access185=Rigid%20Industries%20IGNITE%20BACKUP%20KIT,%20STANDARD%20-%202016-2017%20TACOMA-TUNDRA-4RUNNER%20" />
	<cfset package.link3 = "//admin.iconfigurators.com/admin/configurators/popup/default.cfm?ky=211E299E3E98A7EB2536##/year=2017&model=TACOMA&submodel=BASE&size=16%20Inch%20Wheels&color=Super%20White%20II&wheel=PT758%20Gloss%20Black&currentTab=accessories&tireBrand=BFGoodrich&tire=All-Terrain%20T/A%20KO2&accessCatParentID=4372&access0=Genuine%20Toyota%20Parts%20TAN/BIEGE%20SEATS%20KATZKIN%202014-2017%20TOYOTA%204RUNNER/TACAMO/TUNDRA&access190=Genuine%20Toyota%20Parts%20EXHAUST%20TIP%202017%20TOYOTA%20TACOMA&access160=Genuine%20Toyota%20Parts%20PREDATOR%20TUBE%20STEP%20ASSEMBLY%202017%20TOYOTA%20TACOMA&access185=Rigid%20Industries%20IGNITE%20BACKUP%20KIT,%20STANDARD%20-%202016-2017%20TACOMA-TUNDRA-4RUNNER%20&access180=Rigid%20Industries%20GRILLE%20KIT%202016-2017%20TOYOTA%20TACOMA%20&access245=Rigid%20Industries%20FOG%20LIGHT%20KIT%20SAE%20D/2%20LEVEL%203%20-%202016-2017%20TACOMA%20" />
	<cfset package.price1 = "3,815" />
	<cfset package.price2 = "7,141" />
	<cfset package.price3 = "9,868" />
	<cfset package.AccessoriesList = "3982,5641,5901,5903,5933,5934,5935,5936,5945,5937,5938,5708" />
	<cfset package.excludeList1 = "5935,5945,5936,5708,5937,5938,5903" />
	<cfset package.excludeList2 = "5708,5937,5938,5903" />
	<cfset package.excludeList3 = "3941,5901,5641" />
	<cfset package.accList1 = "3982,5641,5933,5934,5901" />
	<cfset package.accList2 = "3982,5641,5933,5934,5901,5935,5945,5936" />
	<cfset package.acclist3 = "3982,5933,5934,5935,5945,5708,5937,5938,5903" />
	<cfset package.wheel = 10597>
	<cfset package.wheelPartNumber = 'PT758-35170-02'>
	<cfset package.tire = 3849>
	<cfset package.tirePartNumber = '66255'>
</cfif>
<!--- 4Runner ---->
<cfif StructKeyExists(url,'vehicle') AND url.vehicle eq '4runner'>
	<cfset package.vehicleTitle = "2018 4Runner" />
	<cfset package.vehicleImage = "package/4runner-Level-0.png" />
	<cfset package.vehicleImageStart = "package/4Runner-Level-" />
	<cfset package.vehicleImageEnd = ".png" />
	<cfset package.kit = "4Runner Package" />
	<cfset package.link0 = "//admin.iconfigurators.com/admin/configurators/popup/default.cfm?ky=211E299E3E98A7EB2536" />
	<cfset package.link1 = "//admin.iconfigurators.com/admin/configurators/popup/default.cfm?ky=211E299E3E98A7EB2536##/year=2017&model=4RUNNER&submodel=SR5&size=17%20Inch%20Wheels&color=Super%20White%20II&wheel=PTR20%20Black&currentTab=accessories&tireBrand=BFGoodrich&tire=Xtreme%20All%20Terrain%20Radial%20&accessCatParentID=4372&access0=Genuine%20Toyota%20Parts%20TRD%20PRO%20SHIFT%20KNOB%20AUTOMATIC%202017%20TOYOTA%20TUNDRA-4RUNNER&access180=Genuine%20Toyota%20Parts%20TRD%20PRO%20GRILL%20KIT%202014%20-%202017%20TOYOTA%204RUNNER%20&access190=Genuine%20Toyota%20Parts%20EXHAUST%20TIP%202017%20TOYOTA%204RUNNER&access220=Rigid%20Industries%20LEVEL%201%20FOG%20LIGHT%20KIT%202014-2017%20TOYOTA%204RUNNER%20RIGID%20INDUSTRIES%20" />
	<cfset package.link2 = "//admin.iconfigurators.com/admin/configurators/popup/default.cfm?ky=211E299E3E98A7EB2536##/year=2017&model=4RUNNER&submodel=SR5&size=17%20Inch%20Wheels&color=Super%20White%20II&wheel=PTR20%20Black&currentTab=accessories&tireBrand=BFGoodrich&tire=Xtreme%20All%20Terrain%20Radial%20&accessCatParentID=4372&access0=Genuine%20Toyota%20Parts%20TAN/BIEGE%20SEATS%20KATZKIN%202014-2017%20TOYOTA%204RUNNER/TACAMO/TUNDRA&access180=Genuine%20Toyota%20Parts%20TRD%20PRO%20GRILL%20KIT%202014%20-%202017%20TOYOTA%204RUNNER%20&access190=Genuine%20Toyota%20Parts%20EXHAUST%20TIP%202017%20TOYOTA%204RUNNER&access220=Rigid%20Industries%20LEVEL%201%20FOG%20LIGHT%20KIT%202014-2017%20TOYOTA%204RUNNER%20RIGID%20INDUSTRIES%20&access160=Genuine%20Toyota%20Parts%20PREDATOR%20TUBE%20STEP%20ASSEMBLY%202017%20TOYOTA%204RUNNER%20" />
	<cfset package.link3 = "//admin.iconfigurators.com/admin/configurators/popup/default.cfm?ky=211E299E3E98A7EB2536##/year=2017&model=4RUNNER&submodel=SR5&size=17%20Inch%20Wheels&color=Super%20White%20II&wheel=PTR20%20Black&currentTab=accessories&tireBrand=BFGoodrich&tire=Xtreme%20All%20Terrain%20Radial%20&accessCatParentID=4372&access0=Genuine%20Toyota%20Parts%20TAN/BIEGE%20SEATS%20KATZKIN%202014-2017%20TOYOTA%204RUNNER/TACAMO/TUNDRA&access190=Genuine%20Toyota%20Parts%20EXHAUST%20TIP%202017%20TOYOTA%204RUNNER&access160=Genuine%20Toyota%20Parts%20PREDATOR%20TUBE%20STEP%20ASSEMBLY%202017%20TOYOTA%204RUNNER%20&access220=Rigid%20Industries%20FOG%20LIGHT%20KIT%20LEVEL%202%202014-2017%20TOYOTA%204RUNNER%20-%20RIGID%20INDUSTRIES&access180=Rigid%20Industries%20GRILLE%20KIT%202014-2017%20TOYOTA%204RUNNER%20" />
	<cfset package.price1 = "4,665" />
	<cfset package.price2 = "8,074" />
	<cfset package.price3 = "9,142" />
	<cfset package.AccessoriesList = "3982,5904,5939,5943,5967,5965,5944,5945,5947,5936" />
	<cfset package.excludeList1 = "5944,5945,5947,5936,5965" />
	<cfset package.excludeList2 = "5947,5936,5965" />
	<cfset package.excludeList3 = "5967" />
	<cfset package.accList1 = "3982,5967,5939,5943,5904" />
	<cfset package.accList2 = "3982,5967,5939,5943,5904,5944,5945" />
	<cfset package.acclist3 = "3982,5939,5943,5904,5944,5945,5947,5936" />
	<cfset package.wheel = 10597>
	<cfset package.wheelPartNumber = 'PT758-35170-02'>
	<cfset package.tire = 3849>
	<cfset package.tirePartNumber = '66255'>
</cfif>


<!--- Camry ---->
<cfif StructKeyExists(url,'vehicle') AND url.vehicle eq 'camry'>
	<cfset package.vehicleTitle = "2018 Camry" />
	<cfset package.vehicleImage = "package/camry-Level-0.png" />
	<cfset package.vehicleImageStart = "package/Camry-Level-" />
	<cfset package.vehicleImageEnd = ".png" />
	<cfset package.kit = "Camry Package" />
	<cfset package.kitTitle = ["Crown Interior Chill","Crown Stealth","Crown Racing"] />
	<cfset package.link0 = "//admin.iconfigurators.com/admin/configurators/popup/default.cfm?ky=211E299E3E98A7EB2536" />
	<cfset package.link1 = "//admin.iconfigurators.com/admin/configurators/popup/default.cfm?ky=211E299E3E98A7EB2536##/year=2017&model=4RUNNER&submodel=SR5&size=17%20Inch%20Wheels&color=Super%20White%20II&wheel=PTR20%20Black&currentTab=accessories&tireBrand=BFGoodrich&tire=Xtreme%20All%20Terrain%20Radial%20&accessCatParentID=4372&access0=Genuine%20Toyota%20Parts%20TRD%20PRO%20SHIFT%20KNOB%20AUTOMATIC%202017%20TOYOTA%20TUNDRA-4RUNNER&access180=Genuine%20Toyota%20Parts%20TRD%20PRO%20GRILL%20KIT%202014%20-%202017%20TOYOTA%204RUNNER%20&access190=Genuine%20Toyota%20Parts%20EXHAUST%20TIP%202017%20TOYOTA%204RUNNER&access220=Rigid%20Industries%20LEVEL%201%20FOG%20LIGHT%20KIT%202014-2017%20TOYOTA%204RUNNER%20RIGID%20INDUSTRIES%20" />
	<cfset package.link2 = "//admin.iconfigurators.com/admin/configurators/popup/default.cfm?ky=211E299E3E98A7EB2536##/year=2017&model=4RUNNER&submodel=SR5&size=17%20Inch%20Wheels&color=Super%20White%20II&wheel=PTR20%20Black&currentTab=accessories&tireBrand=BFGoodrich&tire=Xtreme%20All%20Terrain%20Radial%20&accessCatParentID=4372&access0=Genuine%20Toyota%20Parts%20TAN/BIEGE%20SEATS%20KATZKIN%202014-2017%20TOYOTA%204RUNNER/TACAMO/TUNDRA&access180=Genuine%20Toyota%20Parts%20TRD%20PRO%20GRILL%20KIT%202014%20-%202017%20TOYOTA%204RUNNER%20&access190=Genuine%20Toyota%20Parts%20EXHAUST%20TIP%202017%20TOYOTA%204RUNNER&access220=Rigid%20Industries%20LEVEL%201%20FOG%20LIGHT%20KIT%202014-2017%20TOYOTA%204RUNNER%20RIGID%20INDUSTRIES%20&access160=Genuine%20Toyota%20Parts%20PREDATOR%20TUBE%20STEP%20ASSEMBLY%202017%20TOYOTA%204RUNNER%20" />
	<cfset package.link3 = "//admin.iconfigurators.com/admin/configurators/popup/default.cfm?ky=211E299E3E98A7EB2536##/year=2017&model=4RUNNER&submodel=SR5&size=17%20Inch%20Wheels&color=Super%20White%20II&wheel=PTR20%20Black&currentTab=accessories&tireBrand=BFGoodrich&tire=Xtreme%20All%20Terrain%20Radial%20&accessCatParentID=4372&access0=Genuine%20Toyota%20Parts%20TAN/BIEGE%20SEATS%20KATZKIN%202014-2017%20TOYOTA%204RUNNER/TACAMO/TUNDRA&access190=Genuine%20Toyota%20Parts%20EXHAUST%20TIP%202017%20TOYOTA%204RUNNER&access160=Genuine%20Toyota%20Parts%20PREDATOR%20TUBE%20STEP%20ASSEMBLY%202017%20TOYOTA%204RUNNER%20&access220=Rigid%20Industries%20FOG%20LIGHT%20KIT%20LEVEL%202%202014-2017%20TOYOTA%204RUNNER%20-%20RIGID%20INDUSTRIES&access180=Rigid%20Industries%20GRILLE%20KIT%202014-2017%20TOYOTA%204RUNNER%20" />
	<cfset package.price1 = "4,665" />
	<cfset package.price2 = "8,074" />
	<cfset package.price3 = "9,142" />
	<cfset package.AccessoriesList = "3982,5904,5939,5943,5967,5965,5944,5945,5947,5936" />
	<cfset package.excludeList1 = "5944,5945,5947,5936,5965" />
	<cfset package.excludeList2 = "5947,5936,5965" />
	<cfset package.excludeList3 = "5967" />
	<cfset package.accList1 = "3982,5967,5939,5943,5904" />
	<cfset package.accList2 = "3982,5967,5939,5943,5904,5944,5945" />
	<cfset package.acclist3 = "3982,5939,5943,5904,5944,5945,5947,5936" />
	<cfset package.wheel = 10597>
	<cfset package.wheelPartNumber = 'PT758-35170-02'>
	<cfset package.tire = 3849>
	<cfset package.tirePartNumber = '66255'>
</cfif>


<!--- Get Wheel ---->
<cfquery
	name="getWheel"
	datasource="#Application.DSN#">
	SELECT DISTINCT TOP 1 w.intWheelID
		,w.vchrName
		,w.txtDescription
		,w.vchrImage
		,ws.vchrPartNumber
		,(SELECT b.vchrName FROM tbl_brand b WHERE b.intID = w.intbrandID) AS BrandName
	FROM dbo.tbl_wheel w WITH (NOLOCK)
	INNER JOIN dbo.tbl_wheelSpec ws WITH (NOLOCK) ON ws.intWheelID = w.intWheelID
	WHERE w.intWheelID = <cfqueryparam cfsqltype="cf_sql_integer" value="#package.wheel#">
	AND ws.vchrPartNumber = <cfqueryparam cfsqltype="cf_sql_varchar" value="#package.wheelPartNumber#">
	ORDER BY ws.vchrPartNumber
</cfquery>

<!--- Get Tire ---->
<cfquery
	name="getTire"
	datasource="#Application.DSN#">
	SELECT t.intTireID
		,t.vchrName
		,t.vchrImage
		,t.txtDescription
		,ts.vchrPartNumber
		,(SELECT b.vchrName FROM tbl_brand b WHERE b.intID = t.intbrandID) AS BrandName
	FROM dbo.tbl_tire t WITH (NOLOCK)
	INNER JOIN dbo.tbl_tireSpec ts WITH (NOLOCK) ON ts.intTireID = t.intTireID
	WHERE t.intTireID = <cfqueryparam cfsqltype="cf_sql_integer" value="#package.tire#">
	AND ts.vchrPartNumber = <cfqueryparam cfsqltype="cf_sql_varchar" value="#package.TirePartNumber#">
</cfquery>

<!--- Get Accessoreis ---->
<cfquery
	name="getAccessories"
	datasource="#Application.DSN#">
	SELECT intAccessoryID
		,vchrName
		,vchrMPN
		,vchrImageMed
		,txtDescription
		,(SELECT b.vchrName FROM tbl_brand b WHERE b.intID = a.intbrandID) AS BrandName
	FROM dbo.tbl_accessories a WITH (NOLOCK)
	WHERE a.intAccessoryID IN (#package.accessoriesList#)

	ORDER BY
	  CASE intAccessoryID
      <!--- keeps same order as originally intended --->
      <cfloop list="#package.accessoriesList#" from="1" to="#listLen(package.accessoriesList,',')#" index="i" item="accNumber">
        <cfoutput>WHEN '#accNumber#' THEN #i#</cfoutput>
      </cfloop>
    END, intAccessoryID

</cfquery>

<!---
<cfdump var="#package.accessorieslist#" />
<cfdump var="#getAccessories#" />
<cfdump var="#getWheel#" />
<cfdump var="#getTire#" />
<cfdump var="#getAccessories#" />
--->
<cfmodule template="/modules/layout.cfm"
	title="#Application.TITLE#"
	page="home"
    keywords=""
	description="">

<!---- Package Header -------->
<section class="package-header hide-for-small-only">
	<cfoutput>
		<div class="row">
			<div class="column small-2"><div class="package-header-title"><h1>#package.vehicleTitle#</h1></div></div>
		</div>
		<div class="row expanded collapse">
			<div class="column medium-12"><a class="package-header-link" href="#package.link0#"><div class="package-header-image"><img src="/images/#package.vehicleImage#" <cfif StructKeyExists(url,'vehicle') AND url.vehicle eq 'camry'>style="max-width:800px; width:100%;"</cfif> /></div></a></div>
		</div>
	</cfoutput>
</section>

<!--- Package Menu ----------->
<section class="package-menu">
	<div class="row" data-equalizer="menu" data-equalize-on="medium">
		<div class="column large-6 menu-container" data-equalizer-watch="menu">
			<ul data-equalizer="menu-item">
				<li><a href="/" data-equalizer-watch="menu-item">vehicles</a></li>
				<li><a data-equalizer-watch="menu-item">packages</a></li>
				<cfoutput><li><a href="#package.link0#" data-equalizer-watch="menu-item">build your own</a></li></cfoutput>
			</ul>
		</div>
		<div class="column large-6 help-container">
			<cfoutput>
				<div class="menu-title show-for-small-only"><h1>#package.vehicleTitle#</h1></div>
				<div class="menu-help" data-equalizer-watch="menu">Choose From 3 Packages</div>
			</cfoutput>
		</div>
	</div>
</section>

<!--- Packages -------------->
<section class="packages">
	<div class="row" data-equalizer="packages" data-equalize-on="medium">
		<cfloop from="1" to="3" index="i">
			<!--- kit variables --->
			<cfset kitPrice = 'package.price' & #i#>
			<cfset excludes = 'package.excludeList' & #i#>
			<cfset kitLink = 'package.link' & #i#>
			<cfset kitAccessories = 'package.accList' & #i#><!--- for ajax --->

		<div class="column medium-4">
			<div class="package">
				<div class="package-button-container">
					<cfoutput>
						<a href="javascript:void(0);" class="package-button button hide-for-small-only" data-image="/images/#package.vehicleImageStart##i##package.vehicleImageEnd#" data-packageTitle="#package.kit#" data-package="#i#" data-link="#evaluate(#kitLink#)#" data-wheel="#package.wheelPartNumber#" data-wheelid="#package.wheel#" data-tire="#package.tirePartNumber#" data-accessories="#evaluate(#kitAccessories#)#">Show On Vehicle</a>
						<img src="/images/#package.vehicleImageStart##i##package.vehicleImageEnd#" class="show-for-small-only" />
					</cfoutput>
				</div>
				<div class="package-header">
				    <cfif StructKeyExists(url,'vehicle') AND url.vehicle eq 'camry'>
				      <cfoutput><div class="package-title"><h2>#package.kitTitle[i]#</h2></div></cfoutput>
				    <cfelse>
					  <cfoutput><div class="package-title"><h2>#package.kit# #i#</h2></div></cfoutput>
                    </cfif>
					<cfoutput><div class="package-price"><h2>$#evaluate(#kitPrice#)#</h2><small>msrp</small></div></cfoutput>
				</div>
				<div class="package-contents" data-equalizer-watch="packages">
					<cfoutput>
						<div class="show-for-small-only">
							<a data-toggle="package-contents-list-#i#" class="activate-toggle"><i class="fa fa-plus-square" aria-hidden="true"></i> Details</a>
						</div>
						<div id="package-contents-list-#i#" class="package-contents-list" data-toggler data-animate="hinge-in-from-top hinge-out-from-top">
							<ul>
							    <cfif StructKeyExists(url,'vehicle') AND url.vehicle eq 'camry'>
							       <cfif i eq 1> <!--- crown interior chill --->
                                     <li><span>Carpeted Floor Mat(s) and Truck Mat Package</span></li>
                                     <li><span>Illuminated Door Sills</span></li>
                                     <li><span>Tint</span></li>
                                     <li><span>Coin Holder/Ashtray Cup</span></li>
                                     <li><span>Hideaway Cargo Net</span></li>
                                     <li><span>Universal Tablet Holder </span></li>
                                   <cfelseif i eq 2>
                                     <li><span>Window Tint</span></li>
                                     <li><span>Chrome Delete</span></li>
                                     <li><span>Emblem Blackout</span></li>
                                     <li><span>Spoiler wrap</span></li>
                                     <li><span>ICW Racing - Black Banshee Wheels</span></li>
                                     <li><span>Black Wheel Locks</span></li>
                                   <cfelse>
                                     <li><span>Hood Stripe</span></li>
                                     <li><span>Mudguards</span></li>
                                     <li><span>Lower Rocker Applique</span></li>
                                     <li><span>ICW Racing - Kamakaze Wheels</span></li>
                                     <li><span>Chrome Wheel Locks</span></li>
							       </cfif>
							    <cfelse>
								<cfoutput query="getWheel">
                                    <li><span>#vchrName#</span></li>
								</cfoutput>
								<cfoutput query="getTire">
                                    <li><span>#vchrName#</span></li>
								</cfoutput>
								<cfoutput query="getAccessories">
									<cfif i eq 1>
										<cfif !listFindNoCase("#evaluate(#excludes#)#", intAccessoryID)>
                                            <li><span>#vchrName#</span></li>
										</cfif>
									<cfelseif i eq 2>
										<cfif !listFindNoCase("#evaluate(#excludes#)#", intAccessoryID)>
                                            <li><span>#vchrName#</span></li>
										</cfif>
									<cfelseif i eq 3>
										<cfif !listFindNoCase("#evaluate(#excludes#)#", intAccessoryID)>
                                            <li><span>#vchrName#</span></li>
										</cfif>
									</cfif>

								</cfoutput>
                                </cfif>
							</ul>
						</div>
					</cfoutput>
					
				</div>
				<div class="package-links">
					<cfoutput>
					<a href="javascript:void(0);" class="button contactUs" data-packagekit="#package.kit# #i#">Contact Us</a>
					</cfoutput>
					<!--- not used for longo ---->
					<!---
					<a href="#">Finance It</a>
					--->
				</div>
			</div>
		</div>
		</cfloop>
	</div>
</section>

<cfif StructKeyExists(url,'vehicle') AND url.vehicle neq 'camry'>
<section class="package-slider">
	<div class="row">
		<cfoutput>
			<div class="column small-12 text-center"><div class="package-slider-header">#package.kit# <span></span> includes:</div></div>
		</cfoutput>
	</div>
	<div class="row small-up-3 package-list-slider" id="package-list">
		<cfoutput query="getWheel">
			<div class="column">
				<div class="package-slider-images">
					<div class="package-slider-image">
						<img src="#Application.SYSTEMIMAGEPATH#wheels/med/#vchrImage#" />
					</div>
					<div class="package-slider-description">
						<div class="row">
							<div class="column small-12"><div class="package-slider-description-title"><span>#brandname#</span><br />#vchrName#</div></div>
							<div class="column small-12"><div class="package-slider-description-partnumber">(#vchrPartNumber#)</div></div>
							<cfif len(txtDescription) gt 0>
								<div class="column small-12" >
									<div class="package-slider-description-contents">
										<p>#txtDescription#</p>
										<div class="description-expand"><a href="javascript:void(0)" class="expand">More...</a></div>
									</div>
								</div>
							</cfif>
						</div>

					</div>
				</div>
			</div>
		</cfoutput>
		<cfoutput query="getTire">
			<div class="column">
				<div class="package-slider-images">
					<div class="package-slider-image">
						<img src="#Application.SYSTEMIMAGEPATH#tires/med/#vchrImage#" />
					</div>
					<div class="package-slider-description">
						<div class="row">
							<div class="column small-12"><div class="package-slider-description-title"><span>#brandname#</span><br />#vchrName#</div></div>
							<div class="column small-12"><div class="package-slider-description-partnumber">(#vchrPartNumber#)</div></div>
							<cfif len(txtDescription) gt 0>
								<div class="column small-12" >
									<div class="package-slider-description-contents">
										<p>#txtDescription#</p>
										<div class="description-expand"><a href="javascript:void(0)" class="expand">More...</a></div>
									</div>
								</div>
							</cfif>
						</div>

					</div>
				</div>
			</div>
		</cfoutput>
		<cfoutput query="getAccessories">
			<div class="column">
				<div class="package-slider-images">
					<div class="package-slider-image">
						<img src="#Application.SYSTEMIMAGEPATH#accessories/med/#vchrImageMed#" />
					</div>
					<div class="package-slider-description">
						<div class="row">
							<div class="column small-12"><div class="package-slider-description-title"><span>#brandname#</span><br />#vchrName#</div></div>
							<div class="column small-12"><div class="package-slider-description-partnumber">(#vchrMPN#)</div></div>
							<cfif len(txtDescription) gt 0>
								<div class="column small-12" >
									<div class="package-slider-description-contents">
										<p>#txtDescription#</p>
										<div class="description-expand"><a href="javascript:void(0)" class="expand">More...</a></div>
									</div>
								</div>
							</cfif>
						</div>

					</div>
				</div>
			</div>
		</cfoutput>
	</div>
		
		
		
		<!----
		<cfloop from="1" to="3" index="j">
		<div class="column medium-4">
		
		<div class="package-slider-images">
			<div class="package-slider-image">
				<img src="/images/package-image-sample.jpg" />
			</div>
			<div class="package-slider-description">
				<div class="row" data-equalizer="slider">
					<div class="column small-6"><div class="package-slider-description-title" data-equalizer-watch="slider">TRD Grille</div></div>
					<div class="column small-6"><div class="package-slider-description-partnumber" data-equalizer-watch="slider">(63100-0C260-C0)</div></div>
					<div class="column small-12"><div class="package-slider-description-contents">description, description, description</div></div>
				</div>
				
			</div>
		</div>
		</div>
		</cfloop>
		--->
	</div>
</section>
</cfif>
	<cfinclude template="/includes/more-info.cfm">
</cfmodule>


   


<cfparam name="url.error" default="">

<cfset message = "">

<cfif StructKeyExists(url, "pdf") AND url.pdf gt 0>
    <cfquery
        name="GetCategories"
        datasource="#Application.DSN#">
        SELECT vchrHighRes
            ,vchrName
            ,intID
            ,vchrThumbnail
        FROM tbl_category
        WHERE intParent = <cfqueryparam cfsqltype="cf_sql_integer" value="#url.pdf#">
    </cfquery>

    <cfquery
        name = "GetCat"
        datasource = "#Application.DSN#"
        cachedwithin="#CreateTimeSpan(0,1,0,0)#">
        SELECT vchrTitle
            ,vchrThumbnail
            ,vchrHighRes
            ,vchrName
            ,intParent
        FROM tbl_category WITH (NOLOCK)
        WHERE intID = <cfqueryparam value = "#url.pdf#" CFSQLType="cf_sql_integer">
    </cfquery>
<cfelseif StructKeyExists(url, "id") AND url.id gt 0>
    <!--- get all the wheel images that aren't an "angle" shot --->
    <cfquery
        name = "GetWheels"
        datasource = "#Application.DSN#"
        cachedwithin="#CreateTimeSpan(0,1,0,0)#">
        SELECT wi.vchrName
            ,wi.vchrHighRes
            ,wi.vchrthumbnail
            ,wi.intLugs
            ,f.vchrName AS FinishName
            ,c.intID
            ,c.vchrName AS catName
    		,c.vchrTitle AS catImage
    		,c.intSort
        FROM tbl_wheelimage wi WITH (NOLOCK)
    	INNER JOIN tbl_wheel w WITH (NOLOCK) ON w.intWheelID = wi.intWheelID
    	INNER JOIN tbl_wheelcategory wc WITH (NOLOCK) ON wc.intWheelID = w.intWheelID
    	INNER JOIN tbl_category c WITH (NOLOCK) ON c.intID = wc.intCategoryId
        INNER JOIN tbl_finish f WITH (NOLOCK) ON f.intFinishID = wi.intFinishID
        WHERE c.intAccountID = <cfqueryparam cfsqltype="cf_sql_integer" value="#Application.ACCOUNTID#">
            AND c.intid = <cfqueryparam cfsqltype="cf_sql_integer" value="#url.id#">
          AND wi.vchrHighRes <> ''
        ORDER BY wi.vchrName, wi.intlugs, wi.blnDefault DESC
    </cfquery>

    <cfquery
        name = "GetCat"
        datasource = "#Application.DSN#"
        cachedwithin="#CreateTimeSpan(0,1,0,0)#">
        SELECT vchrTitle
            ,vchrThumbnail
            ,vchrHighRes
            ,intParent
        FROM tbl_category WITH (NOLOCK)
        WHERE intID = <cfqueryparam value = "#url.id#" CFSQLType="cf_sql_integer">
    </cfquery>

<cfelse>
    <cfquery
        name="GetWheelCategories"
        datasource="#Application.dsn#">
        SELECT c.vchrName
            ,c.intID
            ,c.vchrThumbnail
        FROM tbl_category c WITH (NOLOCK)
        WHERE blnDisplay = 1
            AND intParent = #Application.WHEELSID#
        ORDER BY intSort, vchrName
    </cfquery>
</cfif>

<cfmodule template="/modules/layout.cfm"
	title="Downloads - #Application.TITLE#"
	page="downloads"
    keywords=""
	description="">

<div id="downloads">
    <div class="parallax">
        <div class="img" style="background-image: url(/images/banner-mkw-download.jpg);"></div>
        <h1 class="text-uppercase white">
            <cfif StructKeyExists(url, "pdf") AND url.pdf gt 0>
                <cfoutput>#Replace(GetCat.vchrName, 'DOWNLOADS', '')#</cfoutput> Downloads
            <cfelseif StructKeyExists(url, "id") AND url.id gt 0>
                <cfoutput>#GetWheels.catName#</cfoutput> Downloads
            <cfelse>
    			Media Downloads
            </cfif>
		</h1>
    </div>
    <cfif !isDefined('url.id')>
        <cfoutput><div class="row text-center description">#Application.TITLE2# high resolution image downloads are for dealers &amp; distributors use only.</div></cfoutput>
    </cfif>
        <cfif StructKeyExists(url, "pdf") AND url.pdf gt 0>
            <div class="padded brand-container text-center">
                <div class="row small-up-2 medium-up-4 large-up-6">
                    <cfoutput query="GetCategories">
                        <div class="column">
        					<a href="#Application.SYSTEMIMAGEPATH#categories/highres/#vchrHighRes#" target="_blank">
                                <img src="#Application.SYSTEMIMAGEPATH#categories/thumb/#vchrThumbnail#"/>
                                <h6>#vchrName#</h6>
                            </a>
                        </div>
                    </cfoutput>
                </div>
            </div>
        <cfelseif StructKeyExists(url, "id") AND url.id gt 0>
            <div class="brand-container short padded">
                <div class="row">
                    <cfoutput query="GetCat">
                        <div class="logo small-12 columns text-center">
                            <cfif url.id eq '5432'>
                                <img src="/images/avenue-logo-black.png"/>
                            </cfif>
                            <cfif url.id eq '5430'>
                                <img src="/images/mkw-logo-black.png"/>
                            </cfif>
                            <cfif url.id eq '5431'>
                                <img src="/images/mkw-offroad-logo-black.png"/>
                            </cfif>
                            <!--<img src="#Application.SYSTEMIMAGEPATH#categories/thumb/#vchrThumbnail#"/>-->
                            <h5 class="text-uppercase"><a href="#Application.SYSTEMIMAGEPATH#categories/highres/#vchrHighres#" target="_blank">Download Logos</a></h5>
                        </div>
                    </cfoutput>
                </div>
            </div>

            <div class="brand-container text-center">
                <div class="row small-up-2 medium-up-4 large-up-6">
                    <cfoutput query="GetWheels">
                        <div class="column">
        					<a href="#Application.SYSTEMIMAGEPATH#wheels/highres/#vchrHighRes#" target="_blank">
                                <img src="#Application.SYSTEMIMAGEPATH#wheels/thumb/#vchrThumbnail#"/>
                                <h6>#vchrName#</h6>
                            </a>
                        </div>
                    </cfoutput>
                </div>
            </div>
        <cfelse>
            <div class="padded row small-up-2 medium-up-3 text-center">
                <cfoutput query="GetWheelCategories">
                    <div class="short padded column">
                        <a href="/#Application.PrettyURL(vchrName,'media', intID)#">
                            <cfif vchrName eq 'avenue'>
                                <img src="/images/avenue-logo-black.png"/>
                            </cfif>
                            <cfif vchrName eq 'mkw'>
                                <img src="/images/mkw-logo-black.png"/>
                            </cfif>
                            <cfif vchrName eq 'mkw offroad'>
                                <img src="/images/mkw-offroad-logo-black.png"/>
                            </cfif>
                            <!--<img src="#Application.SYSTEMIMAGEPATH#categories/thumb/#vchrThumbnail#"/>-->
                            <h6 class="short padded">#vchrName# Downloads</h6>
                        </a>
                    </div>
                </cfoutput>
            </div>
        </cfif>
   
</div>

</cfmodule>

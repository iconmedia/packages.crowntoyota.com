<cfset getAccessoriesBrands = createObject("component", "models.accessories").getAccessoriesBrands()>

<cfquery
	name = "getPageDetails"
	datasource = "#Application.DSN#">
	SELECT vchrTitle
		,vchrName
		,vchrMarketingTitle AS catType
		,vchrMarketingKeywords
		,vchrMarketingDescription
	FROM tbl_category WITH (NOLOCK)
	WHERE intAccountID = <cfqueryparam cfsqltype="cf_sql_integer" value="#Application.settings.intAccountID#">
		<cfif StructKeyExists(url, "cat") AND url.cat gt 0>
			AND vchrMarketingTitle = <cfqueryparam cfsqltype="cf_sql_varchar" value="#url.cat#">
		<cfelse>
			AND intID = <cfqueryparam cfsqltype="cf_sql_integer" value="#Application.WHEELSID#">
		</cfif>
</cfquery>

<cfset seoTitle = "Accessory Brands">

<cfmodule template="/modules/layout.cfm"
	title="#seoTitle# - #Application.TITLE#"
	page="accessories"
    keywords=""
	description="">

<div id="accessories">
    <div class="banner-top text-center">
        <div class="wheel-banner parallax">
            <cfoutput>
                <cfif getPageDetails.vchrTitle neq ''>
                    <div class="img" style="background-image: url(#Application.SYSTEMIMAGEPATH#categories/title/#getPageDetails.vchrTitle#)"></div>
                <cfelse>
                    <div class="img" style="background-image: url('/images/header/banner-wheels.jpg')"></div>
                </cfif>
                <h1 class="wheel-header heading text-uppercase white parallax">
                    Ford truck accessories
                </h1>
            </cfoutput>
        </div>
    </div>
    <div class="brand-container padded row">
        <div class="products-list small-up-2 medium-up-3 large-up-4 text-center">
            <cfoutput query="getAccessoriesBrands">
                <div class="column">
                    <a href="/#Application.PrettyURL(vchrName,'accessories', intID)#">
                        <div class="main hover-item text-center">
                            <img src="#Application.SYSTEMIMAGEPATH#brands/#vchrThumbnail#" alt="#vchrName#" />
                        </div>
                        <h6 class="font-normal text-uppercase">#vchrName#</h6>
                    </a>
                </div>
            </cfoutput>
        </div>
    </div>
</div>
</cfmodule>

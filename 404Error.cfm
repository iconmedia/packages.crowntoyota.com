<cfmodule
	template="/modules/layout.cfm"
	title="404 ERROR - #Application.TITLE#"
    description ="404 ERROR - #Application.TITLE#"
    keywords="404 ERROR - #Application.TITLE#"
	page="404">

<div class="row">
    <h2>404 Error - Page Not Found</h2>
</div>

</cfmodule>
